function tau = inverseDynamics(ip, xm, qd, qdd, fext)
g = 9.81;
if nargin < 5
    fext{1} = zeros(6,1);
    fext{2} = zeros(6,1);
    fext{3} = zeros(6,1);
    fext{4} = zeros(6,1);
    fext{5} = zeros(6,1);
end
%
% Pass 1. Forward propagate velocities and accelerations
%

% Link 'link1'
link1_a = xm.fr_link1_XM_fr_base0(:,6) * g; % TODO hide 6
link1_a(3) = link1_a(3) + qdd(1);
link1_v = [0;0;qd(1);0;0;0];
w2 = qd(1)*qd(1);
vxIv = [-ip.lf_link1.tensor6D(2,3) * w2; ...
         ip.lf_link1.tensor6D(1,2) * w2; ...
         0; ...
         ip.lf_link1.tensor6D(2,6) * w2; ...
         ip.lf_link1.tensor6D(3,4) * w2; ...
         0];
link1_f = -fext{1} + ip.lf_link1.tensor6D * link1_a + vxIv;

% Link 'link2'
link2_v = ((xm.fr_link2_XM_fr_link1) * link1_v);
link2_v(6) = link2_v(6) + qd(2);

vcross = vcross_mx(link2_v);

link2_a = xm.fr_link2_XM_fr_link1 * link1_a + (vcross(:,6) * qd(2));
link2_a(6) = link2_a(6) + qdd(2);

link2_f = -fext{2} + ip.lf_link2.tensor6D * link2_a + (-vcross' * ip.lf_link2.tensor6D * link2_v);

% Link 'link3'
link3_v = ((xm.fr_link3_XM_fr_link2) * link2_v);
link3_v(3) = link3_v(3) + qd(3);

vcross = vcross_mx(link3_v);

link3_a = xm.fr_link3_XM_fr_link2 * link2_a + (vcross(:,3) * qd(3));
link3_a(3) = link3_a(3) + qdd(3);

link3_f = -fext{3} + ip.lf_link3.tensor6D * link3_a + (-vcross' * ip.lf_link3.tensor6D * link3_v);

% Link 'link4'
link4_v = ((xm.fr_link4_XM_fr_link3) * link3_v);
link4_v(6) = link4_v(6) + qd(4);

vcross = vcross_mx(link4_v);

link4_a = xm.fr_link4_XM_fr_link3 * link3_a + (vcross(:,6) * qd(4));
link4_a(6) = link4_a(6) + qdd(4);

link4_f = -fext{4} + ip.lf_link4.tensor6D * link4_a + (-vcross' * ip.lf_link4.tensor6D * link4_v);

% Link 'link5'
link5_v = ((xm.fr_link5_XM_fr_link4) * link4_v);
link5_v(3) = link5_v(3) + qd(5);

vcross = vcross_mx(link5_v);

link5_a = xm.fr_link5_XM_fr_link4 * link4_a + (vcross(:,3) * qd(5));
link5_a(3) = link5_a(3) + qdd(5);

link5_f = -fext{5} + ip.lf_link5.tensor6D * link5_a + (-vcross' * ip.lf_link5.tensor6D * link5_v);

%
% Pass 2. Compute the joint torques while back propagating the spatial forces
%
tau = zeros(5,1);

% Link 'link5'
tau(5) = link5_f(3);
link4_f = link4_f + xm.fr_link5_XM_fr_link4' * link5_f;

% Link 'link4'
tau(4) = link4_f(6);
link3_f = link3_f + xm.fr_link4_XM_fr_link3' * link4_f;

% Link 'link3'
tau(3) = link3_f(3);
link2_f = link2_f + xm.fr_link3_XM_fr_link2' * link3_f;

% Link 'link2'
tau(2) = link2_f(6);
link1_f = link1_f + xm.fr_link2_XM_fr_link1' * link2_f;

% Link 'link1'
tau(1) = link1_f(3);
end

function vc = vcross_mx(v)
    vc = [   0    -v(3)  v(2)   0     0     0    ;
             v(3)  0    -v(1)   0     0     0    ;
            -v(2)  v(1)  0      0     0     0    ;
             0    -v(6)  v(5)   0    -v(3)  v(2) ;
             v(6)  0    -v(4)   v(3)  0    -v(1) ;
            -v(5)  v(4)  0     -v(2)  v(1)  0    ];
end
