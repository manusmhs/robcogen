#ifndef RCG_FANCY_FORWARD_DYNAMICS_H_
#define RCG_FANCY_FORWARD_DYNAMICS_H_

#include <iit/rbd/rbd.h>
#include <iit/rbd/InertiaMatrix.h>
#include <iit/rbd/utils.h>

#include "declarations.h"
#include "transforms.h"
#include "inertia_properties.h"
#include "link_data_map.h"

namespace Fancy {
namespace rcg {

/**
 * The Forward Dynamics routine for the robot Fancy.
 *
 * The parameters common to most of the methods are the joint status \c q, the
 * joint velocities \c qd and the joint forces \c tau. The accelerations \c qdd
 * will be filled with the computed values. Overloaded methods without the \c q
 * parameter use the current configuration of the robot; they are provided for
 * the sake of efficiency, in case the kinematics transforms of the robot have
 * already been updated elsewhere with the most recent configuration (eg by a
 * call to setJointStatus()), so that it would be useless to compute them again.
 */
class ForwardDynamics {
public:
    typedef LinkDataMap<Force> ExtForces;

    /**
     * Default constructor
     * \param in the inertia properties of the links
     * \param tr the container of all the spatial motion transforms of
     *     the robot Fancy, which will be used by this instance
     *     to compute the dynamics.
     */
    ForwardDynamics(InertiaProperties& in, MotionTransforms& tr);
    /** \name Forward dynamics
     * The Articulated-Body-Algorithm to compute the joint accelerations
     */ ///@{
    /**
     * \param qdd the joint accelerations vector (output parameter).
     * \param q the joint status vector
     * \param qd the joint velocities vector
     * \param tau the joint forces (torque or force)
     * \param fext the external forces, optional. Each force must be
     *              expressed in the reference frame of the link it is
     *              exerted on.
     */
    void fd(
        JointState& qdd, // output parameter
        const JointState& q, const JointState& qd, const JointState& tau, const ExtForces& fext = zeroExtForces);
    void fd(
        JointState& qdd, // output parameter
        const JointState& qd, const JointState& tau, const ExtForces& fext = zeroExtForces);
    ///@}

    /** Updates all the kinematics transforms used by this instance. */
    void setJointStatus(const JointState& q) const;

private:
    InertiaProperties* inertiaProps;
    MotionTransforms* motionTransforms;

    Matrix66 vcross; // support variable
    Matrix66 Ia_p;   // support variable, articulated inertia in the case of a prismatic joint
    Matrix66 Ia_r;   // support variable, articulated inertia in the case of a revolute joint

    // Link 'link1' :
    Matrix66 link1_AI;
    Velocity link1_a;
    Velocity link1_v;
    Velocity link1_c;
    Force    link1_p;

    Column6 link1_U;
    Scalar link1_D;
    Scalar link1_u;
    // Link 'link2' :
    Matrix66 link2_AI;
    Velocity link2_a;
    Velocity link2_v;
    Velocity link2_c;
    Force    link2_p;

    Column6 link2_U;
    Scalar link2_D;
    Scalar link2_u;
    // Link 'link3' :
    Matrix66 link3_AI;
    Velocity link3_a;
    Velocity link3_v;
    Velocity link3_c;
    Force    link3_p;

    Column6 link3_U;
    Scalar link3_D;
    Scalar link3_u;
    // Link 'link4' :
    Matrix66 link4_AI;
    Velocity link4_a;
    Velocity link4_v;
    Velocity link4_c;
    Force    link4_p;

    Column6 link4_U;
    Scalar link4_D;
    Scalar link4_u;
    // Link 'link5' :
    Matrix66 link5_AI;
    Velocity link5_a;
    Velocity link5_v;
    Velocity link5_c;
    Force    link5_p;

    Column6 link5_U;
    Scalar link5_D;
    Scalar link5_u;
private:
    static const ExtForces zeroExtForces;
};

inline void ForwardDynamics::setJointStatus(const JointState& q) const {
    (motionTransforms-> fr_link1_X_fr_base0)(q);
    (motionTransforms-> fr_link2_X_fr_link1)(q);
    (motionTransforms-> fr_link3_X_fr_link2)(q);
    (motionTransforms-> fr_link4_X_fr_link3)(q);
    (motionTransforms-> fr_link5_X_fr_link4)(q);
}

inline void ForwardDynamics::fd(
    JointState& qdd,
    const JointState& q,
    const JointState& qd,
    const JointState& tau,
    const ExtForces& fext/* = zeroExtForces */)
{
    setJointStatus(q);
    fd(qdd, qd, tau, fext);
}

}
}

#endif
