#include "transforms.h"

using namespace Fancy::rcg;

// Constructors

MotionTransforms::MotionTransforms()
 :     fr_base0_X_ee(),
    fr_jD_X_ee(),
    fr_base0_X_fr_jA(),
    fr_base0_X_fr_jB(),
    fr_base0_X_fr_jC(),
    fr_base0_X_fr_jD(),
    fr_base0_X_fr_jE(),
    fr_link1_X_fr_base0(),
    fr_base0_X_fr_link1(),
    fr_link2_X_fr_link1(),
    fr_link1_X_fr_link2(),
    fr_link3_X_fr_link2(),
    fr_link2_X_fr_link3(),
    fr_link4_X_fr_link3(),
    fr_link3_X_fr_link4(),
    fr_link5_X_fr_link4(),
    fr_link4_X_fr_link5()
{}
void MotionTransforms::updateParams(const Params_lengths& v_lengths, const Params_angles& v_angles)
{
    params.lengths = v_lengths;
    params.angles = v_angles;
    params.trig.update();
}

ForceTransforms::ForceTransforms()
 :     fr_base0_X_ee(),
    fr_jD_X_ee(),
    fr_base0_X_fr_jA(),
    fr_base0_X_fr_jB(),
    fr_base0_X_fr_jC(),
    fr_base0_X_fr_jD(),
    fr_base0_X_fr_jE(),
    fr_link1_X_fr_base0(),
    fr_base0_X_fr_link1(),
    fr_link2_X_fr_link1(),
    fr_link1_X_fr_link2(),
    fr_link3_X_fr_link2(),
    fr_link2_X_fr_link3(),
    fr_link4_X_fr_link3(),
    fr_link3_X_fr_link4(),
    fr_link5_X_fr_link4(),
    fr_link4_X_fr_link5()
{}
void ForceTransforms::updateParams(const Params_lengths& v_lengths, const Params_angles& v_angles)
{
    params.lengths = v_lengths;
    params.angles = v_angles;
    params.trig.update();
}

HomogeneousTransforms::HomogeneousTransforms()
 :     fr_base0_X_ee(),
    fr_jD_X_ee(),
    fr_base0_X_fr_jA(),
    fr_base0_X_fr_jB(),
    fr_base0_X_fr_jC(),
    fr_base0_X_fr_jD(),
    fr_base0_X_fr_jE(),
    fr_link1_X_fr_base0(),
    fr_base0_X_fr_link1(),
    fr_link2_X_fr_link1(),
    fr_link1_X_fr_link2(),
    fr_link3_X_fr_link2(),
    fr_link2_X_fr_link3(),
    fr_link4_X_fr_link3(),
    fr_link3_X_fr_link4(),
    fr_link5_X_fr_link4(),
    fr_link4_X_fr_link5()
{}
void HomogeneousTransforms::updateParams(const Params_lengths& v_lengths, const Params_angles& v_angles)
{
    params.lengths = v_lengths;
    params.angles = v_angles;
    params.trig.update();
}

MotionTransforms::Type_fr_base0_X_ee::Type_fr_base0_X_ee()
{
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
}

const MotionTransforms::Type_fr_base0_X_ee& MotionTransforms::Type_fr_base0_X_ee::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = (cos_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jE);
    (*this)(0,1) = (-cos_q_jA * cos_q_jC * sin_q_jE)-(sin_q_jA * cos_q_jE);
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(1,0) = (cos_q_jA * sin_q_jE)+(sin_q_jA * cos_q_jC * cos_q_jE);
    (*this)(1,1) = (cos_q_jA * cos_q_jE)-(sin_q_jA * cos_q_jC * sin_q_jE);
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(2,0) = -sin_q_jC * cos_q_jE;
    (*this)(2,1) = sin_q_jC * sin_q_jE;
    (*this)(2,2) = cos_q_jC;
    (*this)(3,0) = (((-cos_q_jA * cos_q_jC *  q(JD))+( tx_jD * cos_q_jA * sin_q_jC)-( tz_jE * cos_q_jA * cos_q_jC)) * sin_q_jE)+(((-sin_q_jA *  q(JD))+(((-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA)) * sin_q_jC)-( tz_jE * sin_q_jA)) * cos_q_jE);
    (*this)(3,1) = (((sin_q_jA *  q(JD))+(((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * sin_q_jC)+( tz_jE * sin_q_jA)) * sin_q_jE)+(((-cos_q_jA * cos_q_jC *  q(JD))+( tx_jD * cos_q_jA * sin_q_jC)-( tz_jE * cos_q_jA * cos_q_jC)) * cos_q_jE)+( tx_ee * cos_q_jA * sin_q_jC);
    (*this)(3,2) = ( tx_ee * cos_q_jA * cos_q_jC * sin_q_jE)+( tx_ee * sin_q_jA * cos_q_jE)+(((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * cos_q_jC)+( tx_jD * sin_q_jA);
    (*this)(3,3) = (cos_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jE);
    (*this)(3,4) = (-cos_q_jA * cos_q_jC * sin_q_jE)-(sin_q_jA * cos_q_jE);
    (*this)(3,5) = cos_q_jA * sin_q_jC;
    (*this)(4,0) = (((-sin_q_jA * cos_q_jC *  q(JD))+( tx_jD * sin_q_jA * sin_q_jC)-( tz_jE * sin_q_jA * cos_q_jC)) * sin_q_jE)+(((cos_q_jA *  q(JD))+(((-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA)) * sin_q_jC)+( tz_jE * cos_q_jA)) * cos_q_jE);
    (*this)(4,1) = (((-cos_q_jA *  q(JD))+(((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * sin_q_jC)-( tz_jE * cos_q_jA)) * sin_q_jE)+(((-sin_q_jA * cos_q_jC *  q(JD))+( tx_jD * sin_q_jA * sin_q_jC)-( tz_jE * sin_q_jA * cos_q_jC)) * cos_q_jE)+( tx_ee * sin_q_jA * sin_q_jC);
    (*this)(4,2) = ( tx_ee * sin_q_jA * cos_q_jC * sin_q_jE)-( tx_ee * cos_q_jA * cos_q_jE)+(((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * cos_q_jC)-( tx_jD * cos_q_jA);
    (*this)(4,3) = (cos_q_jA * sin_q_jE)+(sin_q_jA * cos_q_jC * cos_q_jE);
    (*this)(4,4) = (cos_q_jA * cos_q_jE)-(sin_q_jA * cos_q_jC * sin_q_jE);
    (*this)(4,5) = sin_q_jA * sin_q_jC;
    (*this)(5,0) = (((sin_q_jC *  q(JD))+( tz_jE * sin_q_jC)+( tx_jD * cos_q_jC)+ tx_jB) * sin_q_jE)+((- q(JB)- tz_jC) * cos_q_jC * cos_q_jE);
    (*this)(5,1) = (( q(JB)+ tz_jC) * cos_q_jC * sin_q_jE)+(((sin_q_jC *  q(JD))+( tz_jE * sin_q_jC)+( tx_jD * cos_q_jC)+ tx_jB) * cos_q_jE)+( tx_ee * cos_q_jC);
    (*this)(5,2) = ((- q(JB)- tz_jC) * sin_q_jC)-( tx_ee * sin_q_jC * sin_q_jE);
    (*this)(5,3) = -sin_q_jC * cos_q_jE;
    (*this)(5,4) = sin_q_jC * sin_q_jE;
    (*this)(5,5) = cos_q_jC;
    return *this;
}
MotionTransforms::Type_fr_jD_X_ee::Type_fr_jD_X_ee()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) =  tx_ee;    // Maxima DSL: _k__tx_ee
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_jD_X_ee& MotionTransforms::Type_fr_jD_X_ee::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = -sin_q_jE;
    (*this)(1,0) = sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(3,0) = (- q(JD)- tz_jE) * sin_q_jE;
    (*this)(3,1) = (- q(JD)- tz_jE) * cos_q_jE;
    (*this)(3,2) =  tx_ee * sin_q_jE;
    (*this)(3,3) = cos_q_jE;
    (*this)(3,4) = -sin_q_jE;
    (*this)(4,0) = ( q(JD)+ tz_jE) * cos_q_jE;
    (*this)(4,1) = (- q(JD)- tz_jE) * sin_q_jE;
    (*this)(4,2) = - tx_ee * cos_q_jE;
    (*this)(4,3) = sin_q_jE;
    (*this)(4,4) = cos_q_jE;
    return *this;
}
MotionTransforms::Type_fr_base0_X_fr_jA::Type_fr_base0_X_fr_jA()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_base0_X_fr_jA& MotionTransforms::Type_fr_base0_X_fr_jA::update(const state_t& q)
{
    return *this;
}
MotionTransforms::Type_fr_base0_X_fr_jB::Type_fr_base0_X_fr_jB()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_base0_X_fr_jB& MotionTransforms::Type_fr_base0_X_fr_jB::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,2) = -sin_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,2) = cos_q_jA;
    (*this)(3,1) = - tx_jB * sin_q_jA;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,5) = -sin_q_jA;
    (*this)(4,1) =  tx_jB * cos_q_jA;
    (*this)(4,3) = sin_q_jA;
    (*this)(4,5) = cos_q_jA;
    return *this;
}
MotionTransforms::Type_fr_base0_X_fr_jC::Type_fr_base0_X_fr_jC()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_base0_X_fr_jC& MotionTransforms::Type_fr_base0_X_fr_jC::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,2) = -sin_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,2) = cos_q_jA;
    (*this)(3,1) = (-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA);
    (*this)(3,3) = cos_q_jA;
    (*this)(3,5) = -sin_q_jA;
    (*this)(4,1) = (-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(4,3) = sin_q_jA;
    (*this)(4,5) = cos_q_jA;
    (*this)(5,0) = - q(JB)- tz_jC;
    return *this;
}
MotionTransforms::Type_fr_base0_X_fr_jD::Type_fr_base0_X_fr_jD()
{
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(5,4) = 0.0;
}

const MotionTransforms::Type_fr_base0_X_fr_jD& MotionTransforms::Type_fr_base0_X_fr_jD::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jA * cos_q_jC;
    (*this)(0,1) = -sin_q_jA;
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(1,0) = sin_q_jA * cos_q_jC;
    (*this)(1,1) = cos_q_jA;
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(2,0) = -sin_q_jC;
    (*this)(2,2) = cos_q_jC;
    (*this)(3,0) = ((-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA)) * sin_q_jC;
    (*this)(3,1) =  tx_jD * cos_q_jA * sin_q_jC;
    (*this)(3,2) = (((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * cos_q_jC)+( tx_jD * sin_q_jA);
    (*this)(3,3) = cos_q_jA * cos_q_jC;
    (*this)(3,4) = -sin_q_jA;
    (*this)(3,5) = cos_q_jA * sin_q_jC;
    (*this)(4,0) = ((-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA)) * sin_q_jC;
    (*this)(4,1) =  tx_jD * sin_q_jA * sin_q_jC;
    (*this)(4,2) = (((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * cos_q_jC)-( tx_jD * cos_q_jA);
    (*this)(4,3) = sin_q_jA * cos_q_jC;
    (*this)(4,4) = cos_q_jA;
    (*this)(4,5) = sin_q_jA * sin_q_jC;
    (*this)(5,0) = (- q(JB)- tz_jC) * cos_q_jC;
    (*this)(5,1) = ( tx_jD * cos_q_jC)+ tx_jB;
    (*this)(5,2) = (- q(JB)- tz_jC) * sin_q_jC;
    (*this)(5,3) = -sin_q_jC;
    (*this)(5,5) = cos_q_jC;
    return *this;
}
MotionTransforms::Type_fr_base0_X_fr_jE::Type_fr_base0_X_fr_jE()
{
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(5,4) = 0.0;
}

const MotionTransforms::Type_fr_base0_X_fr_jE& MotionTransforms::Type_fr_base0_X_fr_jE::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jA * cos_q_jC;
    (*this)(0,1) = -sin_q_jA;
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(1,0) = sin_q_jA * cos_q_jC;
    (*this)(1,1) = cos_q_jA;
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(2,0) = -sin_q_jC;
    (*this)(2,2) = cos_q_jC;
    (*this)(3,0) = (-sin_q_jA *  q(JD))+(((-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA)) * sin_q_jC)-( tz_jE * sin_q_jA);
    (*this)(3,1) = (-cos_q_jA * cos_q_jC *  q(JD))+( tx_jD * cos_q_jA * sin_q_jC)-( tz_jE * cos_q_jA * cos_q_jC);
    (*this)(3,2) = (((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * cos_q_jC)+( tx_jD * sin_q_jA);
    (*this)(3,3) = cos_q_jA * cos_q_jC;
    (*this)(3,4) = -sin_q_jA;
    (*this)(3,5) = cos_q_jA * sin_q_jC;
    (*this)(4,0) = (cos_q_jA *  q(JD))+(((-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA)) * sin_q_jC)+( tz_jE * cos_q_jA);
    (*this)(4,1) = (-sin_q_jA * cos_q_jC *  q(JD))+( tx_jD * sin_q_jA * sin_q_jC)-( tz_jE * sin_q_jA * cos_q_jC);
    (*this)(4,2) = (((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * cos_q_jC)-( tx_jD * cos_q_jA);
    (*this)(4,3) = sin_q_jA * cos_q_jC;
    (*this)(4,4) = cos_q_jA;
    (*this)(4,5) = sin_q_jA * sin_q_jC;
    (*this)(5,0) = (- q(JB)- tz_jC) * cos_q_jC;
    (*this)(5,1) = (sin_q_jC *  q(JD))+( tz_jE * sin_q_jC)+( tx_jD * cos_q_jC)+ tx_jB;
    (*this)(5,2) = (- q(JB)- tz_jC) * sin_q_jC;
    (*this)(5,3) = -sin_q_jC;
    (*this)(5,5) = cos_q_jC;
    return *this;
}
MotionTransforms::Type_fr_link1_X_fr_base0::Type_fr_link1_X_fr_base0()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_link1_X_fr_base0& MotionTransforms::Type_fr_link1_X_fr_base0::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,1) = sin_q_jA;
    (*this)(1,0) = -sin_q_jA;
    (*this)(1,1) = cos_q_jA;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,4) = sin_q_jA;
    (*this)(4,3) = -sin_q_jA;
    (*this)(4,4) = cos_q_jA;
    return *this;
}
MotionTransforms::Type_fr_base0_X_fr_link1::Type_fr_base0_X_fr_link1()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_base0_X_fr_link1& MotionTransforms::Type_fr_base0_X_fr_link1::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,1) = -sin_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,1) = cos_q_jA;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,4) = -sin_q_jA;
    (*this)(4,3) = sin_q_jA;
    (*this)(4,4) = cos_q_jA;
    return *this;
}
MotionTransforms::Type_fr_link2_X_fr_link1::Type_fr_link2_X_fr_link1()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,1) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_link2_X_fr_link1& MotionTransforms::Type_fr_link2_X_fr_link1::update(const state_t& q)
{
    (*this)(3,2) = - q(JB);
    (*this)(4,0) = - q(JB);
    return *this;
}
MotionTransforms::Type_fr_link1_X_fr_link2::Type_fr_link1_X_fr_link2()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_link1_X_fr_link2& MotionTransforms::Type_fr_link1_X_fr_link2::update(const state_t& q)
{
    (*this)(3,1) = - q(JB);
    (*this)(5,0) = - q(JB);
    return *this;
}
MotionTransforms::Type_fr_link3_X_fr_link2::Type_fr_link3_X_fr_link2()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_link3_X_fr_link2& MotionTransforms::Type_fr_link3_X_fr_link2::update(const state_t& q)
{
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jC;
    (*this)(0,1) = sin_q_jC;
    (*this)(1,0) = -sin_q_jC;
    (*this)(1,1) = cos_q_jC;
    (*this)(3,0) = - tz_jC * sin_q_jC;
    (*this)(3,1) =  tz_jC * cos_q_jC;
    (*this)(3,3) = cos_q_jC;
    (*this)(3,4) = sin_q_jC;
    (*this)(4,0) = - tz_jC * cos_q_jC;
    (*this)(4,1) = - tz_jC * sin_q_jC;
    (*this)(4,3) = -sin_q_jC;
    (*this)(4,4) = cos_q_jC;
    return *this;
}
MotionTransforms::Type_fr_link2_X_fr_link3::Type_fr_link2_X_fr_link3()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_link2_X_fr_link3& MotionTransforms::Type_fr_link2_X_fr_link3::update(const state_t& q)
{
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jC;
    (*this)(0,1) = -sin_q_jC;
    (*this)(1,0) = sin_q_jC;
    (*this)(1,1) = cos_q_jC;
    (*this)(3,0) = - tz_jC * sin_q_jC;
    (*this)(3,1) = - tz_jC * cos_q_jC;
    (*this)(3,3) = cos_q_jC;
    (*this)(3,4) = -sin_q_jC;
    (*this)(4,0) =  tz_jC * cos_q_jC;
    (*this)(4,1) = - tz_jC * sin_q_jC;
    (*this)(4,3) = sin_q_jC;
    (*this)(4,4) = cos_q_jC;
    return *this;
}
MotionTransforms::Type_fr_link4_X_fr_link3::Type_fr_link4_X_fr_link3()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,1) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_link4_X_fr_link3& MotionTransforms::Type_fr_link4_X_fr_link3::update(const state_t& q)
{
    (*this)(3,2) =  q(JD);
    (*this)(4,0) = - q(JD);
    return *this;
}
MotionTransforms::Type_fr_link3_X_fr_link4::Type_fr_link3_X_fr_link4()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const MotionTransforms::Type_fr_link3_X_fr_link4& MotionTransforms::Type_fr_link3_X_fr_link4::update(const state_t& q)
{
    (*this)(3,1) = - q(JD);
    (*this)(5,0) =  q(JD);
    return *this;
}
MotionTransforms::Type_fr_link5_X_fr_link4::Type_fr_link5_X_fr_link4()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_link5_X_fr_link4& MotionTransforms::Type_fr_link5_X_fr_link4::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = sin_q_jE;
    (*this)(1,0) = -sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(3,0) = - tz_jE * sin_q_jE;
    (*this)(3,1) =  tz_jE * cos_q_jE;
    (*this)(3,3) = cos_q_jE;
    (*this)(3,4) = sin_q_jE;
    (*this)(4,0) = - tz_jE * cos_q_jE;
    (*this)(4,1) = - tz_jE * sin_q_jE;
    (*this)(4,3) = -sin_q_jE;
    (*this)(4,4) = cos_q_jE;
    return *this;
}
MotionTransforms::Type_fr_link4_X_fr_link5::Type_fr_link4_X_fr_link5()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const MotionTransforms::Type_fr_link4_X_fr_link5& MotionTransforms::Type_fr_link4_X_fr_link5::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = -sin_q_jE;
    (*this)(1,0) = sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(3,0) = - tz_jE * sin_q_jE;
    (*this)(3,1) = - tz_jE * cos_q_jE;
    (*this)(3,3) = cos_q_jE;
    (*this)(3,4) = -sin_q_jE;
    (*this)(4,0) =  tz_jE * cos_q_jE;
    (*this)(4,1) = - tz_jE * sin_q_jE;
    (*this)(4,3) = sin_q_jE;
    (*this)(4,4) = cos_q_jE;
    return *this;
}

ForceTransforms::Type_fr_base0_X_ee::Type_fr_base0_X_ee()
{
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
}

const ForceTransforms::Type_fr_base0_X_ee& ForceTransforms::Type_fr_base0_X_ee::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = (cos_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jE);
    (*this)(0,1) = (-cos_q_jA * cos_q_jC * sin_q_jE)-(sin_q_jA * cos_q_jE);
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(0,3) = (((-cos_q_jA * cos_q_jC *  q(JD))+( tx_jD * cos_q_jA * sin_q_jC)-( tz_jE * cos_q_jA * cos_q_jC)) * sin_q_jE)+(((-sin_q_jA *  q(JD))+(((-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA)) * sin_q_jC)-( tz_jE * sin_q_jA)) * cos_q_jE);
    (*this)(0,4) = (((sin_q_jA *  q(JD))+(((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * sin_q_jC)+( tz_jE * sin_q_jA)) * sin_q_jE)+(((-cos_q_jA * cos_q_jC *  q(JD))+( tx_jD * cos_q_jA * sin_q_jC)-( tz_jE * cos_q_jA * cos_q_jC)) * cos_q_jE)+( tx_ee * cos_q_jA * sin_q_jC);
    (*this)(0,5) = ( tx_ee * cos_q_jA * cos_q_jC * sin_q_jE)+( tx_ee * sin_q_jA * cos_q_jE)+(((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * cos_q_jC)+( tx_jD * sin_q_jA);
    (*this)(1,0) = (cos_q_jA * sin_q_jE)+(sin_q_jA * cos_q_jC * cos_q_jE);
    (*this)(1,1) = (cos_q_jA * cos_q_jE)-(sin_q_jA * cos_q_jC * sin_q_jE);
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(1,3) = (((-sin_q_jA * cos_q_jC *  q(JD))+( tx_jD * sin_q_jA * sin_q_jC)-( tz_jE * sin_q_jA * cos_q_jC)) * sin_q_jE)+(((cos_q_jA *  q(JD))+(((-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA)) * sin_q_jC)+( tz_jE * cos_q_jA)) * cos_q_jE);
    (*this)(1,4) = (((-cos_q_jA *  q(JD))+(((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * sin_q_jC)-( tz_jE * cos_q_jA)) * sin_q_jE)+(((-sin_q_jA * cos_q_jC *  q(JD))+( tx_jD * sin_q_jA * sin_q_jC)-( tz_jE * sin_q_jA * cos_q_jC)) * cos_q_jE)+( tx_ee * sin_q_jA * sin_q_jC);
    (*this)(1,5) = ( tx_ee * sin_q_jA * cos_q_jC * sin_q_jE)-( tx_ee * cos_q_jA * cos_q_jE)+(((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * cos_q_jC)-( tx_jD * cos_q_jA);
    (*this)(2,0) = -sin_q_jC * cos_q_jE;
    (*this)(2,1) = sin_q_jC * sin_q_jE;
    (*this)(2,2) = cos_q_jC;
    (*this)(2,3) = (((sin_q_jC *  q(JD))+( tz_jE * sin_q_jC)+( tx_jD * cos_q_jC)+ tx_jB) * sin_q_jE)+((- q(JB)- tz_jC) * cos_q_jC * cos_q_jE);
    (*this)(2,4) = (( q(JB)+ tz_jC) * cos_q_jC * sin_q_jE)+(((sin_q_jC *  q(JD))+( tz_jE * sin_q_jC)+( tx_jD * cos_q_jC)+ tx_jB) * cos_q_jE)+( tx_ee * cos_q_jC);
    (*this)(2,5) = ((- q(JB)- tz_jC) * sin_q_jC)-( tx_ee * sin_q_jC * sin_q_jE);
    (*this)(3,3) = (cos_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jE);
    (*this)(3,4) = (-cos_q_jA * cos_q_jC * sin_q_jE)-(sin_q_jA * cos_q_jE);
    (*this)(3,5) = cos_q_jA * sin_q_jC;
    (*this)(4,3) = (cos_q_jA * sin_q_jE)+(sin_q_jA * cos_q_jC * cos_q_jE);
    (*this)(4,4) = (cos_q_jA * cos_q_jE)-(sin_q_jA * cos_q_jC * sin_q_jE);
    (*this)(4,5) = sin_q_jA * sin_q_jC;
    (*this)(5,3) = -sin_q_jC * cos_q_jE;
    (*this)(5,4) = sin_q_jC * sin_q_jE;
    (*this)(5,5) = cos_q_jC;
    return *this;
}
ForceTransforms::Type_fr_jD_X_ee::Type_fr_jD_X_ee()
{
    (*this)(0,2) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) =  tx_ee;    // Maxima DSL: _k__tx_ee
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_jD_X_ee& ForceTransforms::Type_fr_jD_X_ee::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = -sin_q_jE;
    (*this)(0,3) = (- q(JD)- tz_jE) * sin_q_jE;
    (*this)(0,4) = (- q(JD)- tz_jE) * cos_q_jE;
    (*this)(0,5) =  tx_ee * sin_q_jE;
    (*this)(1,0) = sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(1,3) = ( q(JD)+ tz_jE) * cos_q_jE;
    (*this)(1,4) = (- q(JD)- tz_jE) * sin_q_jE;
    (*this)(1,5) = - tx_ee * cos_q_jE;
    (*this)(3,3) = cos_q_jE;
    (*this)(3,4) = -sin_q_jE;
    (*this)(4,3) = sin_q_jE;
    (*this)(4,4) = cos_q_jE;
    return *this;
}
ForceTransforms::Type_fr_base0_X_fr_jA::Type_fr_base0_X_fr_jA()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 1.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_base0_X_fr_jA& ForceTransforms::Type_fr_base0_X_fr_jA::update(const state_t& q)
{
    return *this;
}
ForceTransforms::Type_fr_base0_X_fr_jB::Type_fr_base0_X_fr_jB()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_base0_X_fr_jB& ForceTransforms::Type_fr_base0_X_fr_jB::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,2) = -sin_q_jA;
    (*this)(0,4) = - tx_jB * sin_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,2) = cos_q_jA;
    (*this)(1,4) =  tx_jB * cos_q_jA;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,5) = -sin_q_jA;
    (*this)(4,3) = sin_q_jA;
    (*this)(4,5) = cos_q_jA;
    return *this;
}
ForceTransforms::Type_fr_base0_X_fr_jC::Type_fr_base0_X_fr_jC()
{
    (*this)(0,1) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,4) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_base0_X_fr_jC& ForceTransforms::Type_fr_base0_X_fr_jC::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,2) = -sin_q_jA;
    (*this)(0,4) = (-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA);
    (*this)(1,0) = sin_q_jA;
    (*this)(1,2) = cos_q_jA;
    (*this)(1,4) = (-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(2,3) = - q(JB)- tz_jC;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,5) = -sin_q_jA;
    (*this)(4,3) = sin_q_jA;
    (*this)(4,5) = cos_q_jA;
    return *this;
}
ForceTransforms::Type_fr_base0_X_fr_jD::Type_fr_base0_X_fr_jD()
{
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,4) = 0.0;
}

const ForceTransforms::Type_fr_base0_X_fr_jD& ForceTransforms::Type_fr_base0_X_fr_jD::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jA * cos_q_jC;
    (*this)(0,1) = -sin_q_jA;
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(0,3) = ((-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA)) * sin_q_jC;
    (*this)(0,4) =  tx_jD * cos_q_jA * sin_q_jC;
    (*this)(0,5) = (((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * cos_q_jC)+( tx_jD * sin_q_jA);
    (*this)(1,0) = sin_q_jA * cos_q_jC;
    (*this)(1,1) = cos_q_jA;
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(1,3) = ((-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA)) * sin_q_jC;
    (*this)(1,4) =  tx_jD * sin_q_jA * sin_q_jC;
    (*this)(1,5) = (((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * cos_q_jC)-( tx_jD * cos_q_jA);
    (*this)(2,0) = -sin_q_jC;
    (*this)(2,2) = cos_q_jC;
    (*this)(2,3) = (- q(JB)- tz_jC) * cos_q_jC;
    (*this)(2,4) = ( tx_jD * cos_q_jC)+ tx_jB;
    (*this)(2,5) = (- q(JB)- tz_jC) * sin_q_jC;
    (*this)(3,3) = cos_q_jA * cos_q_jC;
    (*this)(3,4) = -sin_q_jA;
    (*this)(3,5) = cos_q_jA * sin_q_jC;
    (*this)(4,3) = sin_q_jA * cos_q_jC;
    (*this)(4,4) = cos_q_jA;
    (*this)(4,5) = sin_q_jA * sin_q_jC;
    (*this)(5,3) = -sin_q_jC;
    (*this)(5,5) = cos_q_jC;
    return *this;
}
ForceTransforms::Type_fr_base0_X_fr_jE::Type_fr_base0_X_fr_jE()
{
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,4) = 0.0;
}

const ForceTransforms::Type_fr_base0_X_fr_jE& ForceTransforms::Type_fr_base0_X_fr_jE::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jA * cos_q_jC;
    (*this)(0,1) = -sin_q_jA;
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(0,3) = (-sin_q_jA *  q(JD))+(((-cos_q_jA *  q(JB))-( tx_jB * sin_q_jA)-( tz_jC * cos_q_jA)) * sin_q_jC)-( tz_jE * sin_q_jA);
    (*this)(0,4) = (-cos_q_jA * cos_q_jC *  q(JD))+( tx_jD * cos_q_jA * sin_q_jC)-( tz_jE * cos_q_jA * cos_q_jC);
    (*this)(0,5) = (((cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA)) * cos_q_jC)+( tx_jD * sin_q_jA);
    (*this)(1,0) = sin_q_jA * cos_q_jC;
    (*this)(1,1) = cos_q_jA;
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(1,3) = (cos_q_jA *  q(JD))+(((-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA)) * sin_q_jC)+( tz_jE * cos_q_jA);
    (*this)(1,4) = (-sin_q_jA * cos_q_jC *  q(JD))+( tx_jD * sin_q_jA * sin_q_jC)-( tz_jE * sin_q_jA * cos_q_jC);
    (*this)(1,5) = (((sin_q_jA *  q(JB))+( tz_jC * sin_q_jA)-( tx_jB * cos_q_jA)) * cos_q_jC)-( tx_jD * cos_q_jA);
    (*this)(2,0) = -sin_q_jC;
    (*this)(2,2) = cos_q_jC;
    (*this)(2,3) = (- q(JB)- tz_jC) * cos_q_jC;
    (*this)(2,4) = (sin_q_jC *  q(JD))+( tz_jE * sin_q_jC)+( tx_jD * cos_q_jC)+ tx_jB;
    (*this)(2,5) = (- q(JB)- tz_jC) * sin_q_jC;
    (*this)(3,3) = cos_q_jA * cos_q_jC;
    (*this)(3,4) = -sin_q_jA;
    (*this)(3,5) = cos_q_jA * sin_q_jC;
    (*this)(4,3) = sin_q_jA * cos_q_jC;
    (*this)(4,4) = cos_q_jA;
    (*this)(4,5) = sin_q_jA * sin_q_jC;
    (*this)(5,3) = -sin_q_jC;
    (*this)(5,5) = cos_q_jC;
    return *this;
}
ForceTransforms::Type_fr_link1_X_fr_base0::Type_fr_link1_X_fr_base0()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_link1_X_fr_base0& ForceTransforms::Type_fr_link1_X_fr_base0::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,1) = sin_q_jA;
    (*this)(1,0) = -sin_q_jA;
    (*this)(1,1) = cos_q_jA;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,4) = sin_q_jA;
    (*this)(4,3) = -sin_q_jA;
    (*this)(4,4) = cos_q_jA;
    return *this;
}
ForceTransforms::Type_fr_base0_X_fr_link1::Type_fr_base0_X_fr_link1()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_base0_X_fr_link1& ForceTransforms::Type_fr_base0_X_fr_link1::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,1) = -sin_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,1) = cos_q_jA;
    (*this)(3,3) = cos_q_jA;
    (*this)(3,4) = -sin_q_jA;
    (*this)(4,3) = sin_q_jA;
    (*this)(4,4) = cos_q_jA;
    return *this;
}
ForceTransforms::Type_fr_link2_X_fr_link1::Type_fr_link2_X_fr_link1()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,4) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_link2_X_fr_link1& ForceTransforms::Type_fr_link2_X_fr_link1::update(const state_t& q)
{
    (*this)(0,5) = - q(JB);
    (*this)(1,3) = - q(JB);
    return *this;
}
ForceTransforms::Type_fr_link1_X_fr_link2::Type_fr_link1_X_fr_link2()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_link1_X_fr_link2& ForceTransforms::Type_fr_link1_X_fr_link2::update(const state_t& q)
{
    (*this)(0,4) = - q(JB);
    (*this)(2,3) = - q(JB);
    return *this;
}
ForceTransforms::Type_fr_link3_X_fr_link2::Type_fr_link3_X_fr_link2()
{
    (*this)(0,2) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_link3_X_fr_link2& ForceTransforms::Type_fr_link3_X_fr_link2::update(const state_t& q)
{
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jC;
    (*this)(0,1) = sin_q_jC;
    (*this)(0,3) = - tz_jC * sin_q_jC;
    (*this)(0,4) =  tz_jC * cos_q_jC;
    (*this)(1,0) = -sin_q_jC;
    (*this)(1,1) = cos_q_jC;
    (*this)(1,3) = - tz_jC * cos_q_jC;
    (*this)(1,4) = - tz_jC * sin_q_jC;
    (*this)(3,3) = cos_q_jC;
    (*this)(3,4) = sin_q_jC;
    (*this)(4,3) = -sin_q_jC;
    (*this)(4,4) = cos_q_jC;
    return *this;
}
ForceTransforms::Type_fr_link2_X_fr_link3::Type_fr_link2_X_fr_link3()
{
    (*this)(0,2) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_link2_X_fr_link3& ForceTransforms::Type_fr_link2_X_fr_link3::update(const state_t& q)
{
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jC;
    (*this)(0,1) = -sin_q_jC;
    (*this)(0,3) = - tz_jC * sin_q_jC;
    (*this)(0,4) = - tz_jC * cos_q_jC;
    (*this)(1,0) = sin_q_jC;
    (*this)(1,1) = cos_q_jC;
    (*this)(1,3) =  tz_jC * cos_q_jC;
    (*this)(1,4) = - tz_jC * sin_q_jC;
    (*this)(3,3) = cos_q_jC;
    (*this)(3,4) = -sin_q_jC;
    (*this)(4,3) = sin_q_jC;
    (*this)(4,4) = cos_q_jC;
    return *this;
}
ForceTransforms::Type_fr_link4_X_fr_link3::Type_fr_link4_X_fr_link3()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,4) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,4) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = 1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = -1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_link4_X_fr_link3& ForceTransforms::Type_fr_link4_X_fr_link3::update(const state_t& q)
{
    (*this)(0,5) =  q(JD);
    (*this)(1,3) = - q(JD);
    return *this;
}
ForceTransforms::Type_fr_link3_X_fr_link4::Type_fr_link3_X_fr_link4()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(1,4) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
    (*this)(3,4) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,3) = 0.0;
    (*this)(4,4) = 0.0;
    (*this)(4,5) = -1.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 1.0;
    (*this)(5,5) = 0.0;
}

const ForceTransforms::Type_fr_link3_X_fr_link4& ForceTransforms::Type_fr_link3_X_fr_link4::update(const state_t& q)
{
    (*this)(0,4) = - q(JD);
    (*this)(2,3) =  q(JD);
    return *this;
}
ForceTransforms::Type_fr_link5_X_fr_link4::Type_fr_link5_X_fr_link4()
{
    (*this)(0,2) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_link5_X_fr_link4& ForceTransforms::Type_fr_link5_X_fr_link4::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = sin_q_jE;
    (*this)(0,3) = - tz_jE * sin_q_jE;
    (*this)(0,4) =  tz_jE * cos_q_jE;
    (*this)(1,0) = -sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(1,3) = - tz_jE * cos_q_jE;
    (*this)(1,4) = - tz_jE * sin_q_jE;
    (*this)(3,3) = cos_q_jE;
    (*this)(3,4) = sin_q_jE;
    (*this)(4,3) = -sin_q_jE;
    (*this)(4,4) = cos_q_jE;
    return *this;
}
ForceTransforms::Type_fr_link4_X_fr_link5::Type_fr_link4_X_fr_link5()
{
    (*this)(0,2) = 0.0;
    (*this)(0,5) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,5) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(2,4) = 0.0;
    (*this)(2,5) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,5) = 0.0;
    (*this)(4,0) = 0.0;
    (*this)(4,1) = 0.0;
    (*this)(4,2) = 0.0;
    (*this)(4,5) = 0.0;
    (*this)(5,0) = 0.0;
    (*this)(5,1) = 0.0;
    (*this)(5,2) = 0.0;
    (*this)(5,3) = 0.0;
    (*this)(5,4) = 0.0;
    (*this)(5,5) = 1.0;
}

const ForceTransforms::Type_fr_link4_X_fr_link5& ForceTransforms::Type_fr_link4_X_fr_link5::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = -sin_q_jE;
    (*this)(0,3) = - tz_jE * sin_q_jE;
    (*this)(0,4) = - tz_jE * cos_q_jE;
    (*this)(1,0) = sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(1,3) =  tz_jE * cos_q_jE;
    (*this)(1,4) = - tz_jE * sin_q_jE;
    (*this)(3,3) = cos_q_jE;
    (*this)(3,4) = -sin_q_jE;
    (*this)(4,3) = sin_q_jE;
    (*this)(4,4) = cos_q_jE;
    return *this;
}

HomogeneousTransforms::Type_fr_base0_X_ee::Type_fr_base0_X_ee()
{
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_ee& HomogeneousTransforms::Type_fr_base0_X_ee::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = (cos_q_jA * cos_q_jC * cos_q_jE)-(sin_q_jA * sin_q_jE);
    (*this)(0,1) = (-cos_q_jA * cos_q_jC * sin_q_jE)-(sin_q_jA * cos_q_jE);
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(0,3) = (- tx_ee * sin_q_jA * sin_q_jE)+( tx_ee * cos_q_jA * cos_q_jC * cos_q_jE)+(cos_q_jA * sin_q_jC *  q(JD))+( tz_jE * cos_q_jA * sin_q_jC)+( tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(1,0) = (cos_q_jA * sin_q_jE)+(sin_q_jA * cos_q_jC * cos_q_jE);
    (*this)(1,1) = (cos_q_jA * cos_q_jE)-(sin_q_jA * cos_q_jC * sin_q_jE);
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(1,3) = ( tx_ee * cos_q_jA * sin_q_jE)+( tx_ee * sin_q_jA * cos_q_jC * cos_q_jE)+(sin_q_jA * sin_q_jC *  q(JD))+( tz_jE * sin_q_jA * sin_q_jC)+( tx_jD * sin_q_jA * cos_q_jC)+(cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA);
    (*this)(2,0) = -sin_q_jC * cos_q_jE;
    (*this)(2,1) = sin_q_jC * sin_q_jE;
    (*this)(2,2) = cos_q_jC;
    (*this)(2,3) = (- tx_ee * sin_q_jC * cos_q_jE)+(cos_q_jC *  q(JD))-( tx_jD * sin_q_jC)+( tz_jE * cos_q_jC);
    return *this;
}
HomogeneousTransforms::Type_fr_jD_X_ee::Type_fr_jD_X_ee()
{
    (*this)(0,2) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_jD_X_ee& HomogeneousTransforms::Type_fr_jD_X_ee::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = -sin_q_jE;
    (*this)(0,3) =  tx_ee * cos_q_jE;
    (*this)(1,0) = sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    (*this)(1,3) =  tx_ee * sin_q_jE;
    (*this)(2,3) =  q(JD)+ tz_jE;
    return *this;
}
HomogeneousTransforms::Type_fr_base0_X_fr_jA::Type_fr_base0_X_fr_jA()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 1.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_fr_jA& HomogeneousTransforms::Type_fr_base0_X_fr_jA::update(const state_t& q)
{
    return *this;
}
HomogeneousTransforms::Type_fr_base0_X_fr_jB::Type_fr_base0_X_fr_jB()
{
    (*this)(0,1) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_fr_jB& HomogeneousTransforms::Type_fr_base0_X_fr_jB::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,2) = -sin_q_jA;
    (*this)(0,3) =  tx_jB * cos_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,2) = cos_q_jA;
    (*this)(1,3) =  tx_jB * sin_q_jA;
    return *this;
}
HomogeneousTransforms::Type_fr_base0_X_fr_jC::Type_fr_base0_X_fr_jC()
{
    (*this)(0,1) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_fr_jC& HomogeneousTransforms::Type_fr_base0_X_fr_jC::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,2) = -sin_q_jA;
    (*this)(0,3) = (-sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(1,0) = sin_q_jA;
    (*this)(1,2) = cos_q_jA;
    (*this)(1,3) = (cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA);
    return *this;
}
HomogeneousTransforms::Type_fr_base0_X_fr_jD::Type_fr_base0_X_fr_jD()
{
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_fr_jD& HomogeneousTransforms::Type_fr_base0_X_fr_jD::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jA * cos_q_jC;
    (*this)(0,1) = -sin_q_jA;
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(0,3) = ( tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(1,0) = sin_q_jA * cos_q_jC;
    (*this)(1,1) = cos_q_jA;
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(1,3) = ( tx_jD * sin_q_jA * cos_q_jC)+(cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA);
    (*this)(2,0) = -sin_q_jC;
    (*this)(2,2) = cos_q_jC;
    (*this)(2,3) = - tx_jD * sin_q_jC;
    return *this;
}
HomogeneousTransforms::Type_fr_base0_X_fr_jE::Type_fr_base0_X_fr_jE()
{
    (*this)(2,1) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_fr_jE& HomogeneousTransforms::Type_fr_base0_X_fr_jE::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jA * cos_q_jC;
    (*this)(0,1) = -sin_q_jA;
    (*this)(0,2) = cos_q_jA * sin_q_jC;
    (*this)(0,3) = (cos_q_jA * sin_q_jC *  q(JD))+( tz_jE * cos_q_jA * sin_q_jC)+( tx_jD * cos_q_jA * cos_q_jC)-(sin_q_jA *  q(JB))-( tz_jC * sin_q_jA)+( tx_jB * cos_q_jA);
    (*this)(1,0) = sin_q_jA * cos_q_jC;
    (*this)(1,1) = cos_q_jA;
    (*this)(1,2) = sin_q_jA * sin_q_jC;
    (*this)(1,3) = (sin_q_jA * sin_q_jC *  q(JD))+( tz_jE * sin_q_jA * sin_q_jC)+( tx_jD * sin_q_jA * cos_q_jC)+(cos_q_jA *  q(JB))+( tx_jB * sin_q_jA)+( tz_jC * cos_q_jA);
    (*this)(2,0) = -sin_q_jC;
    (*this)(2,2) = cos_q_jC;
    (*this)(2,3) = (cos_q_jC *  q(JD))-( tx_jD * sin_q_jC)+( tz_jE * cos_q_jC);
    return *this;
}
HomogeneousTransforms::Type_fr_link1_X_fr_base0::Type_fr_link1_X_fr_base0()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link1_X_fr_base0& HomogeneousTransforms::Type_fr_link1_X_fr_base0::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,1) = sin_q_jA;
    (*this)(1,0) = -sin_q_jA;
    (*this)(1,1) = cos_q_jA;
    return *this;
}
HomogeneousTransforms::Type_fr_base0_X_fr_link1::Type_fr_base0_X_fr_link1()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_base0_X_fr_link1& HomogeneousTransforms::Type_fr_base0_X_fr_link1::update(const state_t& q)
{
    Scalar sin_q_jA  = ScalarTraits::sin( q(JA) );
    Scalar cos_q_jA  = ScalarTraits::cos( q(JA) );
    (*this)(0,0) = cos_q_jA;
    (*this)(0,1) = -sin_q_jA;
    (*this)(1,0) = sin_q_jA;
    (*this)(1,1) = cos_q_jA;
    return *this;
}
HomogeneousTransforms::Type_fr_link2_X_fr_link1::Type_fr_link2_X_fr_link1()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = - tx_jB;    // Maxima DSL: -_k__tx_jB
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link2_X_fr_link1& HomogeneousTransforms::Type_fr_link2_X_fr_link1::update(const state_t& q)
{
    (*this)(2,3) = - q(JB);
    return *this;
}
HomogeneousTransforms::Type_fr_link1_X_fr_link2::Type_fr_link1_X_fr_link2()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_jB;    // Maxima DSL: _k__tx_jB
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link1_X_fr_link2& HomogeneousTransforms::Type_fr_link1_X_fr_link2::update(const state_t& q)
{
    (*this)(1,3) =  q(JB);
    return *this;
}
HomogeneousTransforms::Type_fr_link3_X_fr_link2::Type_fr_link3_X_fr_link2()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = - tz_jC;    // Maxima DSL: -_k__tz_jC
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link3_X_fr_link2& HomogeneousTransforms::Type_fr_link3_X_fr_link2::update(const state_t& q)
{
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jC;
    (*this)(0,1) = sin_q_jC;
    (*this)(1,0) = -sin_q_jC;
    (*this)(1,1) = cos_q_jC;
    return *this;
}
HomogeneousTransforms::Type_fr_link2_X_fr_link3::Type_fr_link2_X_fr_link3()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) =  tz_jC;    // Maxima DSL: _k__tz_jC
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link2_X_fr_link3& HomogeneousTransforms::Type_fr_link2_X_fr_link3::update(const state_t& q)
{
    Scalar sin_q_jC  = ScalarTraits::sin( q(JC) );
    Scalar cos_q_jC  = ScalarTraits::cos( q(JC) );
    (*this)(0,0) = cos_q_jC;
    (*this)(0,1) = -sin_q_jC;
    (*this)(1,0) = sin_q_jC;
    (*this)(1,1) = cos_q_jC;
    return *this;
}
HomogeneousTransforms::Type_fr_link4_X_fr_link3::Type_fr_link4_X_fr_link3()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) = - tx_jD;    // Maxima DSL: -_k__tx_jD
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = 1.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = -1.0;
    (*this)(2,2) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link4_X_fr_link3& HomogeneousTransforms::Type_fr_link4_X_fr_link3::update(const state_t& q)
{
    (*this)(2,3) = - q(JD);
    return *this;
}
HomogeneousTransforms::Type_fr_link3_X_fr_link4::Type_fr_link3_X_fr_link4()
{
    (*this)(0,0) = 1.0;
    (*this)(0,1) = 0.0;
    (*this)(0,2) = 0.0;
    (*this)(0,3) =  tx_jD;    // Maxima DSL: _k__tx_jD
    (*this)(1,0) = 0.0;
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -1.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 1.0;
    (*this)(2,2) = 0.0;
    (*this)(2,3) = 0.0;
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link3_X_fr_link4& HomogeneousTransforms::Type_fr_link3_X_fr_link4::update(const state_t& q)
{
    (*this)(1,3) = - q(JD);
    return *this;
}
HomogeneousTransforms::Type_fr_link5_X_fr_link4::Type_fr_link5_X_fr_link4()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) = - tz_jE;    // Maxima DSL: -_k__tz_jE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link5_X_fr_link4& HomogeneousTransforms::Type_fr_link5_X_fr_link4::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = sin_q_jE;
    (*this)(1,0) = -sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    return *this;
}
HomogeneousTransforms::Type_fr_link4_X_fr_link5::Type_fr_link4_X_fr_link5()
{
    (*this)(0,2) = 0.0;
    (*this)(0,3) = 0.0;
    (*this)(1,2) = 0.0;
    (*this)(1,3) = 0.0;
    (*this)(2,0) = 0.0;
    (*this)(2,1) = 0.0;
    (*this)(2,2) = 1.0;
    (*this)(2,3) =  tz_jE;    // Maxima DSL: _k__tz_jE
    (*this)(3,0) = 0.0;
    (*this)(3,1) = 0.0;
    (*this)(3,2) = 0.0;
    (*this)(3,3) = 1.0;
}

const HomogeneousTransforms::Type_fr_link4_X_fr_link5& HomogeneousTransforms::Type_fr_link4_X_fr_link5::update(const state_t& q)
{
    Scalar sin_q_jE  = ScalarTraits::sin( q(JE) );
    Scalar cos_q_jE  = ScalarTraits::cos( q(JE) );
    (*this)(0,0) = cos_q_jE;
    (*this)(0,1) = -sin_q_jE;
    (*this)(1,0) = sin_q_jE;
    (*this)(1,1) = cos_q_jE;
    return *this;
}

