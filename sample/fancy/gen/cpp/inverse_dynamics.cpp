#include <iit/rbd/robcogen_commons.h>

#include "inverse_dynamics.h"
#include "inertia_properties.h"
#ifndef EIGEN_NO_DEBUG
    #include <iostream>
#endif
using namespace std;
using namespace iit::rbd;
using namespace Fancy::rcg;

// Initialization of static-const data
const Fancy::rcg::InverseDynamics::ExtForces
Fancy::rcg::InverseDynamics::zeroExtForces(Force::Zero());

Fancy::rcg::InverseDynamics::InverseDynamics(InertiaProperties& inertia, MotionTransforms& transforms) :
    inertiaProps( & inertia ),
    xm( & transforms ),
    link1_I(inertiaProps->getTensor_link1() ),
    link2_I(inertiaProps->getTensor_link2() ),
    link3_I(inertiaProps->getTensor_link3() ),
    link4_I(inertiaProps->getTensor_link4() ),
    link5_I(inertiaProps->getTensor_link5() )
    {
#ifndef EIGEN_NO_DEBUG
    std::cout << "Robot Fancy, InverseDynamics::InverseDynamics()" << std::endl;
    std::cout << "Compiled with Eigen debug active" << std::endl;
#endif
    link1_v.setZero();
    link2_v.setZero();
    link3_v.setZero();
    link4_v.setZero();
    link5_v.setZero();

    vcross.setZero();
}

void Fancy::rcg::InverseDynamics::id(
    JointState& jForces,
    const JointState& qd, const JointState& qdd,
    const ExtForces& fext)
{
    firstPass(qd, qdd, fext);
    secondPass(jForces);
}

void Fancy::rcg::InverseDynamics::G_terms(JointState& jForces)
{
    // Link 'link1'
    link1_a = (xm->fr_link1_X_fr_base0).col(iit::rbd::LZ) * Fancy::rcg::g;
    link1_f = link1_I * link1_a;
    // Link 'link2'
    link2_a = (xm->fr_link2_X_fr_link1) * link1_a;
    link2_f = link2_I * link2_a;
    // Link 'link3'
    link3_a = (xm->fr_link3_X_fr_link2) * link2_a;
    link3_f = link3_I * link3_a;
    // Link 'link4'
    link4_a = (xm->fr_link4_X_fr_link3) * link3_a;
    link4_f = link4_I * link4_a;
    // Link 'link5'
    link5_a = (xm->fr_link5_X_fr_link4) * link4_a;
    link5_f = link5_I * link5_a;

    secondPass(jForces);
}

void Fancy::rcg::InverseDynamics::C_terms(JointState& jForces, const JointState& qd)
{
    // Link 'link1'
    link1_v(iit::rbd::AZ) = qd(JA);   // link1_v = vJ, for the first link of a fixed base robot
    
    link1_f = vxIv(qd(JA), link1_I);
    
    // Link 'link2'
    link2_v = ((xm->fr_link2_X_fr_link1) * link1_v);
    link2_v(iit::rbd::LZ) += qd(JB);
    
    motionCrossProductMx<Scalar>(link2_v, vcross);
    
    link2_a = (vcross.col(iit::rbd::LZ) * qd(JB));
    
    link2_f = link2_I * link2_a + vxIv(link2_v, link2_I);
    
    // Link 'link3'
    link3_v = ((xm->fr_link3_X_fr_link2) * link2_v);
    link3_v(iit::rbd::AZ) += qd(JC);
    
    motionCrossProductMx<Scalar>(link3_v, vcross);
    
    link3_a = (xm->fr_link3_X_fr_link2) * link2_a + vcross.col(iit::rbd::AZ) * qd(JC);
    
    link3_f = link3_I * link3_a + vxIv(link3_v, link3_I);
    
    // Link 'link4'
    link4_v = ((xm->fr_link4_X_fr_link3) * link3_v);
    link4_v(iit::rbd::LZ) += qd(JD);
    
    motionCrossProductMx<Scalar>(link4_v, vcross);
    
    link4_a = (xm->fr_link4_X_fr_link3) * link3_a + vcross.col(iit::rbd::LZ) * qd(JD);
    
    link4_f = link4_I * link4_a + vxIv(link4_v, link4_I);
    
    // Link 'link5'
    link5_v = ((xm->fr_link5_X_fr_link4) * link4_v);
    link5_v(iit::rbd::AZ) += qd(JE);
    
    motionCrossProductMx<Scalar>(link5_v, vcross);
    
    link5_a = (xm->fr_link5_X_fr_link4) * link4_a + vcross.col(iit::rbd::AZ) * qd(JE);
    
    link5_f = link5_I * link5_a + vxIv(link5_v, link5_I);
    

    secondPass(jForces);
}


void Fancy::rcg::InverseDynamics::firstPass(const JointState& qd, const JointState& qdd, const ExtForces& fext)
{
    // First pass, link 'link1'
    link1_a = (xm->fr_link1_X_fr_base0).col(iit::rbd::LZ) * Fancy::rcg::g;
    link1_a(iit::rbd::AZ) += qdd(JA);
    link1_v(iit::rbd::AZ) = qd(JA);   // link1_v = vJ, for the first link of a fixed base robot
    
    link1_f = link1_I * link1_a + vxIv(qd(JA), link1_I)  - fext[LINK1];
    
    // First pass, link 'link2'
    link2_v = ((xm->fr_link2_X_fr_link1) * link1_v);
    link2_v(iit::rbd::LZ) += qd(JB);
    
    motionCrossProductMx<Scalar>(link2_v, vcross);
    
    link2_a = (xm->fr_link2_X_fr_link1) * link1_a + vcross.col(iit::rbd::LZ) * qd(JB);
    link2_a(iit::rbd::LZ) += qdd(JB);
    
    link2_f = link2_I * link2_a + vxIv(link2_v, link2_I) - fext[LINK2];
    
    // First pass, link 'link3'
    link3_v = ((xm->fr_link3_X_fr_link2) * link2_v);
    link3_v(iit::rbd::AZ) += qd(JC);
    
    motionCrossProductMx<Scalar>(link3_v, vcross);
    
    link3_a = (xm->fr_link3_X_fr_link2) * link2_a + vcross.col(iit::rbd::AZ) * qd(JC);
    link3_a(iit::rbd::AZ) += qdd(JC);
    
    link3_f = link3_I * link3_a + vxIv(link3_v, link3_I) - fext[LINK3];
    
    // First pass, link 'link4'
    link4_v = ((xm->fr_link4_X_fr_link3) * link3_v);
    link4_v(iit::rbd::LZ) += qd(JD);
    
    motionCrossProductMx<Scalar>(link4_v, vcross);
    
    link4_a = (xm->fr_link4_X_fr_link3) * link3_a + vcross.col(iit::rbd::LZ) * qd(JD);
    link4_a(iit::rbd::LZ) += qdd(JD);
    
    link4_f = link4_I * link4_a + vxIv(link4_v, link4_I) - fext[LINK4];
    
    // First pass, link 'link5'
    link5_v = ((xm->fr_link5_X_fr_link4) * link4_v);
    link5_v(iit::rbd::AZ) += qd(JE);
    
    motionCrossProductMx<Scalar>(link5_v, vcross);
    
    link5_a = (xm->fr_link5_X_fr_link4) * link4_a + vcross.col(iit::rbd::AZ) * qd(JE);
    link5_a(iit::rbd::AZ) += qdd(JE);
    
    link5_f = link5_I * link5_a + vxIv(link5_v, link5_I) - fext[LINK5];
    
}

void Fancy::rcg::InverseDynamics::secondPass(JointState& jForces)
{
    // Link 'link5'
    jForces(JE) = link5_f(iit::rbd::AZ);
    link4_f += xm->fr_link5_X_fr_link4.transpose() * link5_f;
    // Link 'link4'
    jForces(JD) = link4_f(iit::rbd::LZ);
    link3_f += xm->fr_link4_X_fr_link3.transpose() * link4_f;
    // Link 'link3'
    jForces(JC) = link3_f(iit::rbd::AZ);
    link2_f += xm->fr_link3_X_fr_link2.transpose() * link3_f;
    // Link 'link2'
    jForces(JB) = link2_f(iit::rbd::LZ);
    link1_f += xm->fr_link2_X_fr_link1.transpose() * link2_f;
    // Link 'link1'
    jForces(JA) = link1_f(iit::rbd::AZ);
}
