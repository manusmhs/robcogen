function out = updateHomogeneousTransforms(tr, q, params, consts)

sin_q_LF_HAA = sin( q(1));
cos_q_LF_HAA = cos( q(1));
sin_q_LF_HFE = sin( q(2));
cos_q_LF_HFE = cos( q(2));
sin_q_LF_KFE = sin( q(3));
cos_q_LF_KFE = cos( q(3));
tr.fr_trunk_Xh_LF_foot(1,1) = (cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HFE * sin_q_LF_KFE);
tr.fr_trunk_Xh_LF_foot(1,3) = (cos_q_LF_HFE * sin_q_LF_KFE)+(sin_q_LF_HFE * cos_q_LF_KFE);
tr.fr_trunk_Xh_LF_foot(1,4) = (- consts.tx_LF_foot * cos_q_LF_HFE * sin_q_LF_KFE)-( consts.tx_LF_foot * sin_q_LF_HFE * cos_q_LF_KFE)-( consts.tx_LF_KFE * sin_q_LF_HFE)+ consts.tx_LF_HAA;
tr.fr_trunk_Xh_LF_foot(2,1) = (-sin_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
tr.fr_trunk_Xh_LF_foot(2,2) = cos_q_LF_HAA;
tr.fr_trunk_Xh_LF_foot(2,3) = (sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
tr.fr_trunk_Xh_LF_foot(2,4) = ( consts.tx_LF_foot * sin_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE)-( consts.tx_LF_foot * sin_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-( consts.tx_LF_KFE * sin_q_LF_HAA * cos_q_LF_HFE)-( consts.tx_LF_HFE * sin_q_LF_HAA)+ consts.ty_LF_HAA;
tr.fr_trunk_Xh_LF_foot(3,1) = (-cos_q_LF_HAA * cos_q_LF_HFE * sin_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * cos_q_LF_KFE);
tr.fr_trunk_Xh_LF_foot(3,2) = -sin_q_LF_HAA;
tr.fr_trunk_Xh_LF_foot(3,3) = (cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-(cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE);
tr.fr_trunk_Xh_LF_foot(3,4) = ( consts.tx_LF_foot * cos_q_LF_HAA * sin_q_LF_HFE * sin_q_LF_KFE)-( consts.tx_LF_foot * cos_q_LF_HAA * cos_q_LF_HFE * cos_q_LF_KFE)-( consts.tx_LF_KFE * cos_q_LF_HAA * cos_q_LF_HFE)-( consts.tx_LF_HFE * cos_q_LF_HAA);


sin_q_LF_HAA = sin( q(1));
cos_q_LF_HAA = cos( q(1));
tr.fr_trunk_Xh_fr_LF_HFE(2,1) = -sin_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_HFE(2,3) = cos_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_HFE(2,4) =  consts.ty_LF_HAA-( consts.tx_LF_HFE * sin_q_LF_HAA);
tr.fr_trunk_Xh_fr_LF_HFE(3,1) = -cos_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_HFE(3,3) = -sin_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_HFE(3,4) = - consts.tx_LF_HFE * cos_q_LF_HAA;

sin_q_LF_HAA = sin( q(1));
cos_q_LF_HAA = cos( q(1));
sin_q_LF_HFE = sin( q(2));
cos_q_LF_HFE = cos( q(2));
tr.fr_trunk_Xh_fr_LF_KFE(1,1) = -sin_q_LF_HFE;
tr.fr_trunk_Xh_fr_LF_KFE(1,2) = -cos_q_LF_HFE;
tr.fr_trunk_Xh_fr_LF_KFE(1,4) =  consts.tx_LF_HAA-( consts.tx_LF_KFE * sin_q_LF_HFE);
tr.fr_trunk_Xh_fr_LF_KFE(2,1) = -sin_q_LF_HAA * cos_q_LF_HFE;
tr.fr_trunk_Xh_fr_LF_KFE(2,2) = sin_q_LF_HAA * sin_q_LF_HFE;
tr.fr_trunk_Xh_fr_LF_KFE(2,3) = cos_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_KFE(2,4) = (- consts.tx_LF_KFE * sin_q_LF_HAA * cos_q_LF_HFE)-( consts.tx_LF_HFE * sin_q_LF_HAA)+ consts.ty_LF_HAA;
tr.fr_trunk_Xh_fr_LF_KFE(3,1) = -cos_q_LF_HAA * cos_q_LF_HFE;
tr.fr_trunk_Xh_fr_LF_KFE(3,2) = cos_q_LF_HAA * sin_q_LF_HFE;
tr.fr_trunk_Xh_fr_LF_KFE(3,3) = -sin_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_KFE(3,4) = (- consts.tx_LF_KFE * cos_q_LF_HAA * cos_q_LF_HFE)-( consts.tx_LF_HFE * cos_q_LF_HAA);

sin_q_RF_HAA = sin( q(4));
cos_q_RF_HAA = cos( q(4));
sin_q_RF_HFE = sin( q(5));
cos_q_RF_HFE = cos( q(5));
sin_q_RF_KFE = sin( q(6));
cos_q_RF_KFE = cos( q(6));
tr.fr_trunk_Xh_RF_foot(1,1) = (cos_q_RF_HFE * cos_q_RF_KFE)-(sin_q_RF_HFE * sin_q_RF_KFE);
tr.fr_trunk_Xh_RF_foot(1,3) = (cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HFE * cos_q_RF_KFE);
tr.fr_trunk_Xh_RF_foot(1,4) = (- consts.tx_RF_foot * cos_q_RF_HFE * sin_q_RF_KFE)-( consts.tx_RF_foot * sin_q_RF_HFE * cos_q_RF_KFE)-( consts.tx_RF_KFE * sin_q_RF_HFE)+ consts.tx_RF_HAA;
tr.fr_trunk_Xh_RF_foot(2,1) = (sin_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)+(sin_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
tr.fr_trunk_Xh_RF_foot(2,2) = cos_q_RF_HAA;
tr.fr_trunk_Xh_RF_foot(2,3) = (sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-(sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE);
tr.fr_trunk_Xh_RF_foot(2,4) = (- consts.tx_RF_foot * sin_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)+( consts.tx_RF_foot * sin_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)+( consts.tx_RF_KFE * sin_q_RF_HAA * cos_q_RF_HFE)+( consts.tx_RF_HFE * sin_q_RF_HAA)+ consts.ty_RF_HAA;
tr.fr_trunk_Xh_RF_foot(3,1) = (-cos_q_RF_HAA * cos_q_RF_HFE * sin_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * cos_q_RF_KFE);
tr.fr_trunk_Xh_RF_foot(3,2) = sin_q_RF_HAA;
tr.fr_trunk_Xh_RF_foot(3,3) = (cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-(cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE);
tr.fr_trunk_Xh_RF_foot(3,4) = ( consts.tx_RF_foot * cos_q_RF_HAA * sin_q_RF_HFE * sin_q_RF_KFE)-( consts.tx_RF_foot * cos_q_RF_HAA * cos_q_RF_HFE * cos_q_RF_KFE)-( consts.tx_RF_KFE * cos_q_RF_HAA * cos_q_RF_HFE)-( consts.tx_RF_HFE * cos_q_RF_HAA);


sin_q_RF_HAA = sin( q(4));
cos_q_RF_HAA = cos( q(4));
tr.fr_trunk_Xh_fr_RF_HFE(2,1) = sin_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_HFE(2,3) = cos_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_HFE(2,4) = ( consts.tx_RF_HFE * sin_q_RF_HAA)+ consts.ty_RF_HAA;
tr.fr_trunk_Xh_fr_RF_HFE(3,1) = -cos_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_HFE(3,3) = sin_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_HFE(3,4) = - consts.tx_RF_HFE * cos_q_RF_HAA;

sin_q_RF_HAA = sin( q(4));
cos_q_RF_HAA = cos( q(4));
sin_q_RF_HFE = sin( q(5));
cos_q_RF_HFE = cos( q(5));
tr.fr_trunk_Xh_fr_RF_KFE(1,1) = -sin_q_RF_HFE;
tr.fr_trunk_Xh_fr_RF_KFE(1,2) = -cos_q_RF_HFE;
tr.fr_trunk_Xh_fr_RF_KFE(1,4) =  consts.tx_RF_HAA-( consts.tx_RF_KFE * sin_q_RF_HFE);
tr.fr_trunk_Xh_fr_RF_KFE(2,1) = sin_q_RF_HAA * cos_q_RF_HFE;
tr.fr_trunk_Xh_fr_RF_KFE(2,2) = -sin_q_RF_HAA * sin_q_RF_HFE;
tr.fr_trunk_Xh_fr_RF_KFE(2,3) = cos_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_KFE(2,4) = ( consts.tx_RF_KFE * sin_q_RF_HAA * cos_q_RF_HFE)+( consts.tx_RF_HFE * sin_q_RF_HAA)+ consts.ty_RF_HAA;
tr.fr_trunk_Xh_fr_RF_KFE(3,1) = -cos_q_RF_HAA * cos_q_RF_HFE;
tr.fr_trunk_Xh_fr_RF_KFE(3,2) = cos_q_RF_HAA * sin_q_RF_HFE;
tr.fr_trunk_Xh_fr_RF_KFE(3,3) = sin_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_KFE(3,4) = (- consts.tx_RF_KFE * cos_q_RF_HAA * cos_q_RF_HFE)-( consts.tx_RF_HFE * cos_q_RF_HAA);

sin_q_LH_HAA = sin( q(7));
cos_q_LH_HAA = cos( q(7));
sin_q_LH_HFE = sin( q(8));
cos_q_LH_HFE = cos( q(8));
sin_q_LH_KFE = sin( q(9));
cos_q_LH_KFE = cos( q(9));
tr.fr_trunk_Xh_LH_foot(1,1) = (cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HFE * sin_q_LH_KFE);
tr.fr_trunk_Xh_LH_foot(1,3) = (cos_q_LH_HFE * sin_q_LH_KFE)+(sin_q_LH_HFE * cos_q_LH_KFE);
tr.fr_trunk_Xh_LH_foot(1,4) = (- consts.tx_LH_foot * cos_q_LH_HFE * sin_q_LH_KFE)-( consts.tx_LH_foot * sin_q_LH_HFE * cos_q_LH_KFE)-( consts.tx_LH_KFE * sin_q_LH_HFE)+ consts.tx_LH_HAA;
tr.fr_trunk_Xh_LH_foot(2,1) = (-sin_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
tr.fr_trunk_Xh_LH_foot(2,2) = cos_q_LH_HAA;
tr.fr_trunk_Xh_LH_foot(2,3) = (sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
tr.fr_trunk_Xh_LH_foot(2,4) = ( consts.tx_LH_foot * sin_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE)-( consts.tx_LH_foot * sin_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-( consts.tx_LH_KFE * sin_q_LH_HAA * cos_q_LH_HFE)-( consts.tx_LH_HFE * sin_q_LH_HAA)+ consts.ty_LH_HAA;
tr.fr_trunk_Xh_LH_foot(3,1) = (-cos_q_LH_HAA * cos_q_LH_HFE * sin_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * cos_q_LH_KFE);
tr.fr_trunk_Xh_LH_foot(3,2) = -sin_q_LH_HAA;
tr.fr_trunk_Xh_LH_foot(3,3) = (cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-(cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE);
tr.fr_trunk_Xh_LH_foot(3,4) = ( consts.tx_LH_foot * cos_q_LH_HAA * sin_q_LH_HFE * sin_q_LH_KFE)-( consts.tx_LH_foot * cos_q_LH_HAA * cos_q_LH_HFE * cos_q_LH_KFE)-( consts.tx_LH_KFE * cos_q_LH_HAA * cos_q_LH_HFE)-( consts.tx_LH_HFE * cos_q_LH_HAA);


sin_q_LH_HAA = sin( q(7));
cos_q_LH_HAA = cos( q(7));
tr.fr_trunk_Xh_fr_LH_HFE(2,1) = -sin_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_HFE(2,3) = cos_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_HFE(2,4) =  consts.ty_LH_HAA-( consts.tx_LH_HFE * sin_q_LH_HAA);
tr.fr_trunk_Xh_fr_LH_HFE(3,1) = -cos_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_HFE(3,3) = -sin_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_HFE(3,4) = - consts.tx_LH_HFE * cos_q_LH_HAA;

sin_q_LH_HAA = sin( q(7));
cos_q_LH_HAA = cos( q(7));
sin_q_LH_HFE = sin( q(8));
cos_q_LH_HFE = cos( q(8));
tr.fr_trunk_Xh_fr_LH_KFE(1,1) = -sin_q_LH_HFE;
tr.fr_trunk_Xh_fr_LH_KFE(1,2) = -cos_q_LH_HFE;
tr.fr_trunk_Xh_fr_LH_KFE(1,4) =  consts.tx_LH_HAA-( consts.tx_LH_KFE * sin_q_LH_HFE);
tr.fr_trunk_Xh_fr_LH_KFE(2,1) = -sin_q_LH_HAA * cos_q_LH_HFE;
tr.fr_trunk_Xh_fr_LH_KFE(2,2) = sin_q_LH_HAA * sin_q_LH_HFE;
tr.fr_trunk_Xh_fr_LH_KFE(2,3) = cos_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_KFE(2,4) = (- consts.tx_LH_KFE * sin_q_LH_HAA * cos_q_LH_HFE)-( consts.tx_LH_HFE * sin_q_LH_HAA)+ consts.ty_LH_HAA;
tr.fr_trunk_Xh_fr_LH_KFE(3,1) = -cos_q_LH_HAA * cos_q_LH_HFE;
tr.fr_trunk_Xh_fr_LH_KFE(3,2) = cos_q_LH_HAA * sin_q_LH_HFE;
tr.fr_trunk_Xh_fr_LH_KFE(3,3) = -sin_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_KFE(3,4) = (- consts.tx_LH_KFE * cos_q_LH_HAA * cos_q_LH_HFE)-( consts.tx_LH_HFE * cos_q_LH_HAA);

sin_q_RH_HAA = sin( q(10));
cos_q_RH_HAA = cos( q(10));
sin_q_RH_HFE = sin( q(11));
cos_q_RH_HFE = cos( q(11));
sin_q_RH_KFE = sin( q(12));
cos_q_RH_KFE = cos( q(12));
tr.fr_trunk_Xh_RH_foot(1,1) = (cos_q_RH_HFE * cos_q_RH_KFE)-(sin_q_RH_HFE * sin_q_RH_KFE);
tr.fr_trunk_Xh_RH_foot(1,3) = (cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HFE * cos_q_RH_KFE);
tr.fr_trunk_Xh_RH_foot(1,4) = (- consts.tx_RH_foot * cos_q_RH_HFE * sin_q_RH_KFE)-( consts.tx_RH_foot * sin_q_RH_HFE * cos_q_RH_KFE)-( consts.tx_RH_KFE * sin_q_RH_HFE)+ consts.tx_RH_HAA;
tr.fr_trunk_Xh_RH_foot(2,1) = (sin_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)+(sin_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
tr.fr_trunk_Xh_RH_foot(2,2) = cos_q_RH_HAA;
tr.fr_trunk_Xh_RH_foot(2,3) = (sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-(sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE);
tr.fr_trunk_Xh_RH_foot(2,4) = (- consts.tx_RH_foot * sin_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)+( consts.tx_RH_foot * sin_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)+( consts.tx_RH_KFE * sin_q_RH_HAA * cos_q_RH_HFE)+( consts.tx_RH_HFE * sin_q_RH_HAA)+ consts.ty_RH_HAA;
tr.fr_trunk_Xh_RH_foot(3,1) = (-cos_q_RH_HAA * cos_q_RH_HFE * sin_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * cos_q_RH_KFE);
tr.fr_trunk_Xh_RH_foot(3,2) = sin_q_RH_HAA;
tr.fr_trunk_Xh_RH_foot(3,3) = (cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-(cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE);
tr.fr_trunk_Xh_RH_foot(3,4) = ( consts.tx_RH_foot * cos_q_RH_HAA * sin_q_RH_HFE * sin_q_RH_KFE)-( consts.tx_RH_foot * cos_q_RH_HAA * cos_q_RH_HFE * cos_q_RH_KFE)-( consts.tx_RH_KFE * cos_q_RH_HAA * cos_q_RH_HFE)-( consts.tx_RH_HFE * cos_q_RH_HAA);


sin_q_RH_HAA = sin( q(10));
cos_q_RH_HAA = cos( q(10));
tr.fr_trunk_Xh_fr_RH_HFE(2,1) = sin_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_HFE(2,3) = cos_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_HFE(2,4) = ( consts.tx_RH_HFE * sin_q_RH_HAA)+ consts.ty_RH_HAA;
tr.fr_trunk_Xh_fr_RH_HFE(3,1) = -cos_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_HFE(3,3) = sin_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_HFE(3,4) = - consts.tx_RH_HFE * cos_q_RH_HAA;

sin_q_RH_HAA = sin( q(10));
cos_q_RH_HAA = cos( q(10));
sin_q_RH_HFE = sin( q(11));
cos_q_RH_HFE = cos( q(11));
tr.fr_trunk_Xh_fr_RH_KFE(1,1) = -sin_q_RH_HFE;
tr.fr_trunk_Xh_fr_RH_KFE(1,2) = -cos_q_RH_HFE;
tr.fr_trunk_Xh_fr_RH_KFE(1,4) =  consts.tx_RH_HAA-( consts.tx_RH_KFE * sin_q_RH_HFE);
tr.fr_trunk_Xh_fr_RH_KFE(2,1) = sin_q_RH_HAA * cos_q_RH_HFE;
tr.fr_trunk_Xh_fr_RH_KFE(2,2) = -sin_q_RH_HAA * sin_q_RH_HFE;
tr.fr_trunk_Xh_fr_RH_KFE(2,3) = cos_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_KFE(2,4) = ( consts.tx_RH_KFE * sin_q_RH_HAA * cos_q_RH_HFE)+( consts.tx_RH_HFE * sin_q_RH_HAA)+ consts.ty_RH_HAA;
tr.fr_trunk_Xh_fr_RH_KFE(3,1) = -cos_q_RH_HAA * cos_q_RH_HFE;
tr.fr_trunk_Xh_fr_RH_KFE(3,2) = cos_q_RH_HAA * sin_q_RH_HFE;
tr.fr_trunk_Xh_fr_RH_KFE(3,3) = sin_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_KFE(3,4) = (- consts.tx_RH_KFE * cos_q_RH_HAA * cos_q_RH_HFE)-( consts.tx_RH_HFE * cos_q_RH_HAA);

sin_q_LF_HAA = sin( q(1));
cos_q_LF_HAA = cos( q(1));
tr.fr_LF_hipassembly_Xh_fr_trunk(1,2) = -sin_q_LF_HAA;
tr.fr_LF_hipassembly_Xh_fr_trunk(1,3) = -cos_q_LF_HAA;
tr.fr_LF_hipassembly_Xh_fr_trunk(1,4) =  consts.ty_LF_HAA * sin_q_LF_HAA;
tr.fr_LF_hipassembly_Xh_fr_trunk(2,2) = -cos_q_LF_HAA;
tr.fr_LF_hipassembly_Xh_fr_trunk(2,3) = sin_q_LF_HAA;
tr.fr_LF_hipassembly_Xh_fr_trunk(2,4) =  consts.ty_LF_HAA * cos_q_LF_HAA;

sin_q_LF_HAA = sin( q(1));
cos_q_LF_HAA = cos( q(1));
tr.fr_trunk_Xh_fr_LF_hipassembly(2,1) = -sin_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_hipassembly(2,2) = -cos_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_hipassembly(3,1) = -cos_q_LF_HAA;
tr.fr_trunk_Xh_fr_LF_hipassembly(3,2) = sin_q_LF_HAA;

sin_q_LF_HFE = sin( q(2));
cos_q_LF_HFE = cos( q(2));
tr.fr_LF_upperleg_Xh_fr_LF_hipassembly(1,1) = cos_q_LF_HFE;
tr.fr_LF_upperleg_Xh_fr_LF_hipassembly(1,3) = sin_q_LF_HFE;
tr.fr_LF_upperleg_Xh_fr_LF_hipassembly(1,4) = - consts.tx_LF_HFE * cos_q_LF_HFE;
tr.fr_LF_upperleg_Xh_fr_LF_hipassembly(2,1) = -sin_q_LF_HFE;
tr.fr_LF_upperleg_Xh_fr_LF_hipassembly(2,3) = cos_q_LF_HFE;
tr.fr_LF_upperleg_Xh_fr_LF_hipassembly(2,4) =  consts.tx_LF_HFE * sin_q_LF_HFE;

sin_q_LF_HFE = sin( q(2));
cos_q_LF_HFE = cos( q(2));
tr.fr_LF_hipassembly_Xh_fr_LF_upperleg(1,1) = cos_q_LF_HFE;
tr.fr_LF_hipassembly_Xh_fr_LF_upperleg(1,2) = -sin_q_LF_HFE;
tr.fr_LF_hipassembly_Xh_fr_LF_upperleg(3,1) = sin_q_LF_HFE;
tr.fr_LF_hipassembly_Xh_fr_LF_upperleg(3,2) = cos_q_LF_HFE;

sin_q_LF_KFE = sin( q(3));
cos_q_LF_KFE = cos( q(3));
tr.fr_LF_lowerleg_Xh_fr_LF_upperleg(1,1) = cos_q_LF_KFE;
tr.fr_LF_lowerleg_Xh_fr_LF_upperleg(1,2) = sin_q_LF_KFE;
tr.fr_LF_lowerleg_Xh_fr_LF_upperleg(1,4) = - consts.tx_LF_KFE * cos_q_LF_KFE;
tr.fr_LF_lowerleg_Xh_fr_LF_upperleg(2,1) = -sin_q_LF_KFE;
tr.fr_LF_lowerleg_Xh_fr_LF_upperleg(2,2) = cos_q_LF_KFE;
tr.fr_LF_lowerleg_Xh_fr_LF_upperleg(2,4) =  consts.tx_LF_KFE * sin_q_LF_KFE;

sin_q_LF_KFE = sin( q(3));
cos_q_LF_KFE = cos( q(3));
tr.fr_LF_upperleg_Xh_fr_LF_lowerleg(1,1) = cos_q_LF_KFE;
tr.fr_LF_upperleg_Xh_fr_LF_lowerleg(1,2) = -sin_q_LF_KFE;
tr.fr_LF_upperleg_Xh_fr_LF_lowerleg(2,1) = sin_q_LF_KFE;
tr.fr_LF_upperleg_Xh_fr_LF_lowerleg(2,2) = cos_q_LF_KFE;

sin_q_RF_HAA = sin( q(4));
cos_q_RF_HAA = cos( q(4));
tr.fr_RF_hipassembly_Xh_fr_trunk(1,2) = sin_q_RF_HAA;
tr.fr_RF_hipassembly_Xh_fr_trunk(1,3) = -cos_q_RF_HAA;
tr.fr_RF_hipassembly_Xh_fr_trunk(1,4) = - consts.ty_RF_HAA * sin_q_RF_HAA;
tr.fr_RF_hipassembly_Xh_fr_trunk(2,2) = cos_q_RF_HAA;
tr.fr_RF_hipassembly_Xh_fr_trunk(2,3) = sin_q_RF_HAA;
tr.fr_RF_hipassembly_Xh_fr_trunk(2,4) = - consts.ty_RF_HAA * cos_q_RF_HAA;

sin_q_RF_HAA = sin( q(4));
cos_q_RF_HAA = cos( q(4));
tr.fr_trunk_Xh_fr_RF_hipassembly(2,1) = sin_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_hipassembly(2,2) = cos_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_hipassembly(3,1) = -cos_q_RF_HAA;
tr.fr_trunk_Xh_fr_RF_hipassembly(3,2) = sin_q_RF_HAA;

sin_q_RF_HFE = sin( q(5));
cos_q_RF_HFE = cos( q(5));
tr.fr_RF_upperleg_Xh_fr_RF_hipassembly(1,1) = cos_q_RF_HFE;
tr.fr_RF_upperleg_Xh_fr_RF_hipassembly(1,3) = -sin_q_RF_HFE;
tr.fr_RF_upperleg_Xh_fr_RF_hipassembly(1,4) = - consts.tx_RF_HFE * cos_q_RF_HFE;
tr.fr_RF_upperleg_Xh_fr_RF_hipassembly(2,1) = -sin_q_RF_HFE;
tr.fr_RF_upperleg_Xh_fr_RF_hipassembly(2,3) = -cos_q_RF_HFE;
tr.fr_RF_upperleg_Xh_fr_RF_hipassembly(2,4) =  consts.tx_RF_HFE * sin_q_RF_HFE;

sin_q_RF_HFE = sin( q(5));
cos_q_RF_HFE = cos( q(5));
tr.fr_RF_hipassembly_Xh_fr_RF_upperleg(1,1) = cos_q_RF_HFE;
tr.fr_RF_hipassembly_Xh_fr_RF_upperleg(1,2) = -sin_q_RF_HFE;
tr.fr_RF_hipassembly_Xh_fr_RF_upperleg(3,1) = -sin_q_RF_HFE;
tr.fr_RF_hipassembly_Xh_fr_RF_upperleg(3,2) = -cos_q_RF_HFE;

sin_q_RF_KFE = sin( q(6));
cos_q_RF_KFE = cos( q(6));
tr.fr_RF_lowerleg_Xh_fr_RF_upperleg(1,1) = cos_q_RF_KFE;
tr.fr_RF_lowerleg_Xh_fr_RF_upperleg(1,2) = sin_q_RF_KFE;
tr.fr_RF_lowerleg_Xh_fr_RF_upperleg(1,4) = - consts.tx_RF_KFE * cos_q_RF_KFE;
tr.fr_RF_lowerleg_Xh_fr_RF_upperleg(2,1) = -sin_q_RF_KFE;
tr.fr_RF_lowerleg_Xh_fr_RF_upperleg(2,2) = cos_q_RF_KFE;
tr.fr_RF_lowerleg_Xh_fr_RF_upperleg(2,4) =  consts.tx_RF_KFE * sin_q_RF_KFE;

sin_q_RF_KFE = sin( q(6));
cos_q_RF_KFE = cos( q(6));
tr.fr_RF_upperleg_Xh_fr_RF_lowerleg(1,1) = cos_q_RF_KFE;
tr.fr_RF_upperleg_Xh_fr_RF_lowerleg(1,2) = -sin_q_RF_KFE;
tr.fr_RF_upperleg_Xh_fr_RF_lowerleg(2,1) = sin_q_RF_KFE;
tr.fr_RF_upperleg_Xh_fr_RF_lowerleg(2,2) = cos_q_RF_KFE;

sin_q_LH_HAA = sin( q(7));
cos_q_LH_HAA = cos( q(7));
tr.fr_LH_hipassembly_Xh_fr_trunk(1,2) = -sin_q_LH_HAA;
tr.fr_LH_hipassembly_Xh_fr_trunk(1,3) = -cos_q_LH_HAA;
tr.fr_LH_hipassembly_Xh_fr_trunk(1,4) =  consts.ty_LH_HAA * sin_q_LH_HAA;
tr.fr_LH_hipassembly_Xh_fr_trunk(2,2) = -cos_q_LH_HAA;
tr.fr_LH_hipassembly_Xh_fr_trunk(2,3) = sin_q_LH_HAA;
tr.fr_LH_hipassembly_Xh_fr_trunk(2,4) =  consts.ty_LH_HAA * cos_q_LH_HAA;

sin_q_LH_HAA = sin( q(7));
cos_q_LH_HAA = cos( q(7));
tr.fr_trunk_Xh_fr_LH_hipassembly(2,1) = -sin_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_hipassembly(2,2) = -cos_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_hipassembly(3,1) = -cos_q_LH_HAA;
tr.fr_trunk_Xh_fr_LH_hipassembly(3,2) = sin_q_LH_HAA;

sin_q_LH_HFE = sin( q(8));
cos_q_LH_HFE = cos( q(8));
tr.fr_LH_upperleg_Xh_fr_LH_hipassembly(1,1) = cos_q_LH_HFE;
tr.fr_LH_upperleg_Xh_fr_LH_hipassembly(1,3) = sin_q_LH_HFE;
tr.fr_LH_upperleg_Xh_fr_LH_hipassembly(1,4) = - consts.tx_LH_HFE * cos_q_LH_HFE;
tr.fr_LH_upperleg_Xh_fr_LH_hipassembly(2,1) = -sin_q_LH_HFE;
tr.fr_LH_upperleg_Xh_fr_LH_hipassembly(2,3) = cos_q_LH_HFE;
tr.fr_LH_upperleg_Xh_fr_LH_hipassembly(2,4) =  consts.tx_LH_HFE * sin_q_LH_HFE;

sin_q_LH_HFE = sin( q(8));
cos_q_LH_HFE = cos( q(8));
tr.fr_LH_hipassembly_Xh_fr_LH_upperleg(1,1) = cos_q_LH_HFE;
tr.fr_LH_hipassembly_Xh_fr_LH_upperleg(1,2) = -sin_q_LH_HFE;
tr.fr_LH_hipassembly_Xh_fr_LH_upperleg(3,1) = sin_q_LH_HFE;
tr.fr_LH_hipassembly_Xh_fr_LH_upperleg(3,2) = cos_q_LH_HFE;

sin_q_LH_KFE = sin( q(9));
cos_q_LH_KFE = cos( q(9));
tr.fr_LH_lowerleg_Xh_fr_LH_upperleg(1,1) = cos_q_LH_KFE;
tr.fr_LH_lowerleg_Xh_fr_LH_upperleg(1,2) = sin_q_LH_KFE;
tr.fr_LH_lowerleg_Xh_fr_LH_upperleg(1,4) = - consts.tx_LH_KFE * cos_q_LH_KFE;
tr.fr_LH_lowerleg_Xh_fr_LH_upperleg(2,1) = -sin_q_LH_KFE;
tr.fr_LH_lowerleg_Xh_fr_LH_upperleg(2,2) = cos_q_LH_KFE;
tr.fr_LH_lowerleg_Xh_fr_LH_upperleg(2,4) =  consts.tx_LH_KFE * sin_q_LH_KFE;

sin_q_LH_KFE = sin( q(9));
cos_q_LH_KFE = cos( q(9));
tr.fr_LH_upperleg_Xh_fr_LH_lowerleg(1,1) = cos_q_LH_KFE;
tr.fr_LH_upperleg_Xh_fr_LH_lowerleg(1,2) = -sin_q_LH_KFE;
tr.fr_LH_upperleg_Xh_fr_LH_lowerleg(2,1) = sin_q_LH_KFE;
tr.fr_LH_upperleg_Xh_fr_LH_lowerleg(2,2) = cos_q_LH_KFE;

sin_q_RH_HAA = sin( q(10));
cos_q_RH_HAA = cos( q(10));
tr.fr_RH_hipassembly_Xh_fr_trunk(1,2) = sin_q_RH_HAA;
tr.fr_RH_hipassembly_Xh_fr_trunk(1,3) = -cos_q_RH_HAA;
tr.fr_RH_hipassembly_Xh_fr_trunk(1,4) = - consts.ty_RH_HAA * sin_q_RH_HAA;
tr.fr_RH_hipassembly_Xh_fr_trunk(2,2) = cos_q_RH_HAA;
tr.fr_RH_hipassembly_Xh_fr_trunk(2,3) = sin_q_RH_HAA;
tr.fr_RH_hipassembly_Xh_fr_trunk(2,4) = - consts.ty_RH_HAA * cos_q_RH_HAA;

sin_q_RH_HAA = sin( q(10));
cos_q_RH_HAA = cos( q(10));
tr.fr_trunk_Xh_fr_RH_hipassembly(2,1) = sin_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_hipassembly(2,2) = cos_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_hipassembly(3,1) = -cos_q_RH_HAA;
tr.fr_trunk_Xh_fr_RH_hipassembly(3,2) = sin_q_RH_HAA;

sin_q_RH_HFE = sin( q(11));
cos_q_RH_HFE = cos( q(11));
tr.fr_RH_upperleg_Xh_fr_RH_hipassembly(1,1) = cos_q_RH_HFE;
tr.fr_RH_upperleg_Xh_fr_RH_hipassembly(1,3) = -sin_q_RH_HFE;
tr.fr_RH_upperleg_Xh_fr_RH_hipassembly(1,4) = - consts.tx_RH_HFE * cos_q_RH_HFE;
tr.fr_RH_upperleg_Xh_fr_RH_hipassembly(2,1) = -sin_q_RH_HFE;
tr.fr_RH_upperleg_Xh_fr_RH_hipassembly(2,3) = -cos_q_RH_HFE;
tr.fr_RH_upperleg_Xh_fr_RH_hipassembly(2,4) =  consts.tx_RH_HFE * sin_q_RH_HFE;

sin_q_RH_HFE = sin( q(11));
cos_q_RH_HFE = cos( q(11));
tr.fr_RH_hipassembly_Xh_fr_RH_upperleg(1,1) = cos_q_RH_HFE;
tr.fr_RH_hipassembly_Xh_fr_RH_upperleg(1,2) = -sin_q_RH_HFE;
tr.fr_RH_hipassembly_Xh_fr_RH_upperleg(3,1) = -sin_q_RH_HFE;
tr.fr_RH_hipassembly_Xh_fr_RH_upperleg(3,2) = -cos_q_RH_HFE;

sin_q_RH_KFE = sin( q(12));
cos_q_RH_KFE = cos( q(12));
tr.fr_RH_lowerleg_Xh_fr_RH_upperleg(1,1) = cos_q_RH_KFE;
tr.fr_RH_lowerleg_Xh_fr_RH_upperleg(1,2) = sin_q_RH_KFE;
tr.fr_RH_lowerleg_Xh_fr_RH_upperleg(1,4) = - consts.tx_RH_KFE * cos_q_RH_KFE;
tr.fr_RH_lowerleg_Xh_fr_RH_upperleg(2,1) = -sin_q_RH_KFE;
tr.fr_RH_lowerleg_Xh_fr_RH_upperleg(2,2) = cos_q_RH_KFE;
tr.fr_RH_lowerleg_Xh_fr_RH_upperleg(2,4) =  consts.tx_RH_KFE * sin_q_RH_KFE;

sin_q_RH_KFE = sin( q(12));
cos_q_RH_KFE = cos( q(12));
tr.fr_RH_upperleg_Xh_fr_RH_lowerleg(1,1) = cos_q_RH_KFE;
tr.fr_RH_upperleg_Xh_fr_RH_lowerleg(1,2) = -sin_q_RH_KFE;
tr.fr_RH_upperleg_Xh_fr_RH_lowerleg(2,1) = sin_q_RH_KFE;
tr.fr_RH_upperleg_Xh_fr_RH_lowerleg(2,2) = cos_q_RH_KFE;



out = tr;