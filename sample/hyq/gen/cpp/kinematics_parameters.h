#ifndef _HYQ_PARAMETERS_DEFS_
#define _HYQ_PARAMETERS_DEFS_

#include "rbd_types.h"

namespace HyQ {
namespace rcg {

struct Params_lengths {
    Params_lengths() {
        defaults();
    }
    void defaults() {
    }
};

struct Params_angles {
    Params_angles() {
        defaults();
    }
    void defaults() {
    }
};


}
}
#endif
