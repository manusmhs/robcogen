#include "inertia_properties.h"

using namespace std;
using namespace iit::rbd;

HyQ::rcg::InertiaProperties::InertiaProperties()
{
    com_trunk = Vector3(comx_trunk,comy_trunk,0.0);
    tensor_trunk.fill(
        m_trunk,
        com_trunk,
        Utils::buildInertiaTensor<Scalar>(ix_trunk,iy_trunk,iz_trunk,ixy_trunk,ixz_trunk,iyz_trunk) );

    com_LF_hipassembly = Vector3(comx_LF_hipassembly,0.0,comz_LF_hipassembly);
    tensor_LF_hipassembly.fill(
        m_LF_hipassembly,
        com_LF_hipassembly,
        Utils::buildInertiaTensor<Scalar>(ix_LF_hipassembly,iy_LF_hipassembly,iz_LF_hipassembly,ixy_LF_hipassembly,ixz_LF_hipassembly,iyz_LF_hipassembly) );

    com_LF_upperleg = Vector3(comx_LF_upperleg,comy_LF_upperleg,0.0);
    tensor_LF_upperleg.fill(
        m_LF_upperleg,
        com_LF_upperleg,
        Utils::buildInertiaTensor<Scalar>(ix_LF_upperleg,iy_LF_upperleg,iz_LF_upperleg,ixy_LF_upperleg,ixz_LF_upperleg,iyz_LF_upperleg) );

    com_LF_lowerleg = Vector3(comx_LF_lowerleg,comy_LF_lowerleg,comz_LF_lowerleg);
    tensor_LF_lowerleg.fill(
        m_LF_lowerleg,
        com_LF_lowerleg,
        Utils::buildInertiaTensor<Scalar>(ix_LF_lowerleg,iy_LF_lowerleg,iz_LF_lowerleg,0.0,0.0,0.0) );

    com_RF_hipassembly = Vector3(comx_RF_hipassembly,0.0,comz_RF_hipassembly);
    tensor_RF_hipassembly.fill(
        m_RF_hipassembly,
        com_RF_hipassembly,
        Utils::buildInertiaTensor<Scalar>(ix_RF_hipassembly,iy_RF_hipassembly,iz_RF_hipassembly,ixy_RF_hipassembly,ixz_RF_hipassembly,iyz_RF_hipassembly) );

    com_RF_upperleg = Vector3(comx_RF_upperleg,comy_RF_upperleg,0.0);
    tensor_RF_upperleg.fill(
        m_RF_upperleg,
        com_RF_upperleg,
        Utils::buildInertiaTensor<Scalar>(ix_RF_upperleg,iy_RF_upperleg,iz_RF_upperleg,ixy_RF_upperleg,ixz_RF_upperleg,iyz_RF_upperleg) );

    com_RF_lowerleg = Vector3(comx_RF_lowerleg,comy_RF_lowerleg,comz_RF_lowerleg);
    tensor_RF_lowerleg.fill(
        m_RF_lowerleg,
        com_RF_lowerleg,
        Utils::buildInertiaTensor<Scalar>(ix_RF_lowerleg,iy_RF_lowerleg,iz_RF_lowerleg,0.0,0.0,0.0) );

    com_LH_hipassembly = Vector3(comx_LH_hipassembly,0.0,comz_LH_hipassembly);
    tensor_LH_hipassembly.fill(
        m_LH_hipassembly,
        com_LH_hipassembly,
        Utils::buildInertiaTensor<Scalar>(ix_LH_hipassembly,iy_LH_hipassembly,iz_LH_hipassembly,ixy_LH_hipassembly,ixz_LH_hipassembly,iyz_LH_hipassembly) );

    com_LH_upperleg = Vector3(comx_LH_upperleg,comy_LH_upperleg,0.0);
    tensor_LH_upperleg.fill(
        m_LH_upperleg,
        com_LH_upperleg,
        Utils::buildInertiaTensor<Scalar>(ix_LH_upperleg,iy_LH_upperleg,iz_LH_upperleg,ixy_LH_upperleg,ixz_LH_upperleg,iyz_LH_upperleg) );

    com_LH_lowerleg = Vector3(comx_LH_lowerleg,comy_LH_lowerleg,comz_LH_lowerleg);
    tensor_LH_lowerleg.fill(
        m_LH_lowerleg,
        com_LH_lowerleg,
        Utils::buildInertiaTensor<Scalar>(ix_LH_lowerleg,iy_LH_lowerleg,iz_LH_lowerleg,0.0,0.0,0.0) );

    com_RH_hipassembly = Vector3(comx_RH_hipassembly,0.0,comz_RH_hipassembly);
    tensor_RH_hipassembly.fill(
        m_RH_hipassembly,
        com_RH_hipassembly,
        Utils::buildInertiaTensor<Scalar>(ix_RH_hipassembly,iy_RH_hipassembly,iz_RH_hipassembly,ixy_RH_hipassembly,ixz_RH_hipassembly,iyz_RH_hipassembly) );

    com_RH_upperleg = Vector3(comx_RH_upperleg,comy_RH_upperleg,0.0);
    tensor_RH_upperleg.fill(
        m_RH_upperleg,
        com_RH_upperleg,
        Utils::buildInertiaTensor<Scalar>(ix_RH_upperleg,iy_RH_upperleg,iz_RH_upperleg,ixy_RH_upperleg,ixz_RH_upperleg,iyz_RH_upperleg) );

    com_RH_lowerleg = Vector3(comx_RH_lowerleg,comy_RH_lowerleg,comz_RH_lowerleg);
    tensor_RH_lowerleg.fill(
        m_RH_lowerleg,
        com_RH_lowerleg,
        Utils::buildInertiaTensor<Scalar>(ix_RH_lowerleg,iy_RH_lowerleg,iz_RH_lowerleg,0.0,0.0,0.0) );

}


void HyQ::rcg::InertiaProperties::updateParameters(const RuntimeInertiaParams& fresh)
{
    this-> params = fresh;
}
