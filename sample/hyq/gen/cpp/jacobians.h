#ifndef HYQ_JACOBIANS_H_
#define HYQ_JACOBIANS_H_

#include <iit/rbd/TransformsBase.h>
#include "declarations.h"
#include "kinematics_parameters.h"
#include "transforms.h" // to use the same 'Parameters' struct defined there
#include "model_constants.h"

namespace HyQ {
namespace rcg {

template<int COLS, class M>
class JacobianT : public iit::rbd::JacobianBase<JointState, COLS, M>
{};

/**
 *
 */
class Jacobians
{
    public:
        
        struct Type_fr_trunk_J_LF_foot : public JacobianT<3, Type_fr_trunk_J_LF_foot>
        {
            Type_fr_trunk_J_LF_foot();
            const Type_fr_trunk_J_LF_foot& update(const JointState&);
        };
        
        
        struct Type_fr_trunk_J_RF_foot : public JacobianT<3, Type_fr_trunk_J_RF_foot>
        {
            Type_fr_trunk_J_RF_foot();
            const Type_fr_trunk_J_RF_foot& update(const JointState&);
        };
        
        
        struct Type_fr_trunk_J_LH_foot : public JacobianT<3, Type_fr_trunk_J_LH_foot>
        {
            Type_fr_trunk_J_LH_foot();
            const Type_fr_trunk_J_LH_foot& update(const JointState&);
        };
        
        
        struct Type_fr_trunk_J_RH_foot : public JacobianT<3, Type_fr_trunk_J_RH_foot>
        {
            Type_fr_trunk_J_RH_foot();
            const Type_fr_trunk_J_RH_foot& update(const JointState&);
        };
        
    public:
        Jacobians();
        void updateParameters(const Params_lengths& _lengths, const Params_angles& _angles);
    public:
        Type_fr_trunk_J_LF_foot fr_trunk_J_LF_foot;
        Type_fr_trunk_J_RF_foot fr_trunk_J_RF_foot;
        Type_fr_trunk_J_LH_foot fr_trunk_J_LH_foot;
        Type_fr_trunk_J_RH_foot fr_trunk_J_RH_foot;

    protected:
        Parameters params;

};


}
}

#endif
